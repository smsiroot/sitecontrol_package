<?php

namespace Sitecontrol;

class AffiliateFactory
{
    public static function create($alias)
    {
        $class = 'Sitecontrol\\Affiliate\\' . Text::ucfirst($alias);
        return new $class;
    }
}

interface IAff {
    public static function offersLoad();
    public static function prepareOffer();
}