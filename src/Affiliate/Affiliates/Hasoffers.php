<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Hasoffers
{
    public static function offersLoad($affiliate)
    {
        try {
            $client = new \DevIT\Hasoffers\Client($affiliate->apikey, $affiliate->api_id);
            $api = $client->api('Affiliate\Offer');
            $response = $api->findAll();
        } catch (\DevIT\Hasoffers\Exception $e) {
            Log::warning('error', $e->getRequest(), 'Sitecontrol\Affiliate\Hasoffers', 'offersLoad');
        }
        if ($response->response->httpStatus == '200') {
            if (Text::isArray($response->response->data)) foreach ($response->response->data as $data) $offers[] = Hasoffers::prepareOffer($data->Offer, $affiliate);
            return $offers;
        }
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->preview_url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = $offer->terms_and_conditions;
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = $offer->require_approval;
        $offer_new['offer_status'] = $offer->status;
        $offer_new['categories_original'] = '';//Hasoffers::prepareCategories($offer, $affiliate);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = $offer->currency;
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer->traffic = Hasoffers::getTrafficRules($offer, $affiliate);
        $offer_new['traffic_allowed'] = Hasoffers::prepareTrafficAllowed($offer->traffic);
        $offer_new['traffic_forbidden'] = Hasoffers::prepareTrafficForbidden($offer->traffic);
        $offer_new['geo'] = Hasoffers::prepareGeo($offer, $affiliate);
        $offer_new['actions'] = Hasoffers::prepareActions($offer, json_decode($offer_new['geo']));
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer, $geo)
    {
        $actions['0']['id_original'] = 0;
        $actions['0']['name'] = 'Conversion';
        $actions['0']['hold'] = '';
        if ($offer->payout_type == 'cpa_flat') $actions['0']['payment'] = number_format($offer->default_payout, 2);
        if ($offer->payout_type == 'cpa_percentage') $actions['0']['payment'] = $offer->percent_payout . '%';
        $actions['0']['currency'] = $offer->currency;
        if ($offer->currency == null && $offer->payout_type == 'cpa_flat' && !in_array('RU', $geo) && (count($geo) > 0)) $actions['0']['currency'] = 'USD';
        if ($offer->currency == null && $offer->payout_type == 'cpa_flat' && in_array('RU', $geo) && (count($geo) == 1)) $actions['0']['currency'] = 'RUB';
        $actions['0']['postclick'] = '';
        $actions['0']['geo'] = '';
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer, $affiliate)
    {
        $array = [];
        $client = new \DevIT\Hasoffers\Client($affiliate->apikey, $affiliate->api_id);
        $api = $client->api('Affiliate\Offer');
        try {
            $response = $api->get('getGeoTargeting', ['id' => $offer->id]);
        } catch (\DevIT\Hasoffers\Exception $e) {
            Log::warning('error', $e->getRequest(), 'Sitecontrol\Affiliate\Hasoffers', 'prepareGeo');
        }
        if ($response->response->httpStatus == '200') {
            if (Text::isArray($response->response->data->Countries)) foreach ($response->response->data->Countries as $country) if (!in_array($country->code, $array)) $array[] = mb_strtoupper($country->code, 'utf-8');
            return json_encode($array, JSON_UNESCAPED_UNICODE);
        }
    }

    public static function prepareCategories($offer, $affiliate)
    {
        $array = [];
        $client = new \DevIT\Hasoffers\Client($affiliate->apikey, $affiliate->api_id);
        $api = $client->api('Affiliate\Offer');
        try {
            $response = $api->getCategories(['ids[]' => $offer->id]);
        } catch (\DevIT\Hasoffers\Exception $e) {
            Log::warning('error', $e->getRequest(), 'Sitecontrol\Affiliate\Hasoffers', 'prepareGeo');
        }
        if ($response->response->httpStatus == '200') {
            if (Text::isArray($response->response->data['0']->categories)) foreach ($response->response->data['0']->categories as $cat) if (!in_array($cat->name, $array)) $array[] = $cat->name;
            return json_encode($array, JSON_UNESCAPED_UNICODE);
        }
    }

    public static function prepareTrafficAllowed($rules)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($rules)) foreach ($rules as $rule) {
            if ($rule->action == 'allow') {
                $array[$i]['id_original'] = $rule->rule->id;
                $array[$i]['name'] = $rule->rule->name . ' (' . $rule->rule->description . ')';
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($rules)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($rules)) foreach ($rules as $rule) {
            if ($rule->action != 'allow') {
                $array[$i]['id_original'] = $rule->rule->id;
                $array[$i]['name'] = $rule->rule->name . ' (' . $rule->rule->description . ')';
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function getTrafficRules($offer, $affiliate)
    {
        $client = new \DevIT\Hasoffers\Client($affiliate->apikey, $affiliate->api_id);
        $api = $client->api('Affiliate\Offer\Targeting');
        try {
            $response = $api->getRuleTargetingForOffer(['offer_id' => $offer->id]);
        } catch (\DevIT\Hasoffers\Exception $e) {
            Log::warning('error', $e->getRequest(), 'Sitecontrol\Affiliate\Hasoffers', 'prepareTrafficAllowed');
        }
        if ($response->response->httpStatus == '200') return $response->response->data;
    }
}