<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Clickdealer
{
    public static function offersLoad($affiliate)
    {
        $row_limit = 500;
        $count = simplexml_load_string(file_get_contents('https://partners.clickdealer.com/affiliates/api/1/offers.asmx/OfferFeed?api_key=' . $affiliate->apikey . '&affiliate_id=' . $affiliate->api_id . '&row_limit=1'));
        for ($p = 1; $p <= ceil($count->row_count / $row_limit); $p++) {
            $page = $p - 1;
            $data = simplexml_load_string(file_get_contents('https://partners.clickdealer.com/affiliates/api/1/offers.asmx/OfferFeed?api_key=' . $affiliate->apikey . '&affiliate_id=' . $affiliate->api_id . '&row_limit=' . $row_limit . '&start_at_row=' . ($page * $row_limit)));
            if (Text::isArray($data->offers)) foreach ($data->offers->offer as $offer) {
                $offers[] = Clickdealer::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->offer_name);
        $offer_new['url'] = mb_strtolower($offer->preview_link, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = (string)$offer->description;
        $offer_new['text_rules'] = (string)$offer->restrictions;
        $offer_new['image'] = (string)$offer->preview_link;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = (integer)$offer->offer_id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = (string)$offer->offer_status->status_name;
        $offer_new['categories_original'] = Clickdealer::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = mb_strtoupper($offer->currency, 'utf-8');
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Clickdealer::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Clickdealer::prepareActions($offer);
        $offer_new['geo'] = Clickdealer::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        $array[0]['id_original'] = 0;
        $array[0]['name'] = 'Conversion';
        $array[0]['hold'] = '';
        $payout = Text::match('/([0-9\.]+)/u', $offer->payout);
        $array[0]['payment'] = number_format($payout, 2);
        $array[0]['currency'] = mb_strtoupper($offer->currency, 'utf-8');
        $array[0]['postclick'] = '';
        $array[0]['geo'] = json_decode(Clickdealer::prepareGeo($offer));
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->allowed_countries->country); $i++) {
            if (Text::isArray($offer->allowed_countries->country)) foreach ($offer->allowed_countries->country as $geo) if (!in_array(mb_strtoupper($geo->country_code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->country_code, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        $array[] = (string)$offer->vertical_name;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->allowed_media_types->allowed_media_type)) foreach ($offer->allowed_media_types->allowed_media_type as $rule) {
            $array[$i]['id_original'] = (string)$rule->allowed_media_type_id;
            $array[$i]['name'] = (string)$rule->allowed_media_type_name;
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}