<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Shakespro
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://shakes.pro/index.php?r=offer/offers/json'));
        if (Text::isArray($json)) foreach ($json as $offer) {
            $offers[] = Shakespro::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->landings['0']->url, 'utf-8');
        $offer_new['url_ref'] = $offer->gotolink;
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = '';
        $offer_new['text_rules'] = '';
        $offer_new['image'] = 'http://shakes.pro' . $offer->image;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = '';
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = 'RUB';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = 0;
        $offer_new['landing_price'] = $offer->landing_price;
        $offer_new['traffic_allowed'] = Shakespro::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Shakespro::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Shakespro::prepareActions($offer);
        $offer_new['geo'] = Shakespro::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Shakespro::prepareLandings($offer);
        $offer_new['prelandings'] = Shakespro::preparePrelandings($offer);
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = $offer->ecpc;
        $offer_new['start_time'] = Carbon::parse($offer->created_at)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $i = 0;
        if (Text::isArray($offer->traffic_types)) foreach ($offer->traffic_types as $traffics) {
            if ($traffics->allowed) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->name;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $i = 0;
        if (Text::isArray($offer->traffic_types)) foreach ($offer->traffic_types as $traffics) {
            if (!$traffics->allowed) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->name;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->goals); $i++) {
            $actions[$i]['id_original'] = 0;
            $actions[$i]['name'] = $offer->goals[$i]->type;
            $actions[$i]['hold'] = $offer->hold;
            $actions[$i]['payment'] = $offer->goals[$i]->cost;
            $actions[$i]['currency'] = mb_strtoupper($offer->goals[$i]->currency, 'utf-8');
            $actions[$i]['postclick'] = $offer->postclick;
            $actions[$i]['geo'] = [mb_strtoupper($offer->goals[$i]->geo, 'utf-8')];
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        for ($i = 0; $i < count($offer->landings); $i++) {
            $landings[$i]['name'] = $offer->landings[$i]->title;
            $landings[$i]['url'] = $offer->landings[$i]->url;
            $landings[$i]['type'] = $offer->landings[$i]->type;
            for ($j = 0; $j < count($offer->landings[$i]->transits); $j++) {
                $landings[$i]['transits'][$j]['name'] = $offer->landings[$i]->transits[$j]->title;
                $landings[$i]['transits'][$j]['url'] = $offer->landings[$i]->transits[$j]->url;
            }
        }
        return json_encode($landings, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePrelandings($offer)
    {
        $k = 0;
        for ($i = 0; $i < count($offer->landings); $i++) {
            for ($j = 0; $j < count($offer->landings[$i]->transits); $j++) {
                $prelandings[$k]['name'] = $offer->landings[$i]->transits[$j]->title;
                $prelandings[$k]['url'] = $offer->landings[$i]->transits[$j]->url;
                $k++;
            }
        }
        return json_encode($prelandings, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals); $i++) {
            if (!in_array(mb_strtoupper($offer->goals[$i]->geo, 'utf-8'), $array)) $array[] = mb_strtoupper($offer->goals[$i]->geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}