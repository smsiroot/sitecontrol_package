<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Adcombo
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://api.adcombo.com/offer/info/?per_page=100&api_key=' . $affiliate->apikey));
        for ($p = 1; $p <= ceil($json->total / $json->per_page); $p++) {
            $json = json_decode(file_get_contents('https://api.adcombo.com/offer/info/?per_page=100&api_key=' . $affiliate->apikey . '&page=' . $p));
            if (Text::isArray($json->offers)) foreach ($json->offers as $offer) {
                $offers[] = Adcombo::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        if (isset($offer->description->ru)) $offer_new['text_original'] = $offer->description->ru;
        else $offer_new['text_original'] = $offer->description->en;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = $offer->state;
        $offer_new['categories_original'] = Adcombo::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['geo'] = Adcombo::prepareGeo($offer);
        $offer_new['actions'] = Adcombo::prepareActions($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Adcombo::prepareLandings($offer);
        $offer_new['prelandings'] = Adcombo::preparePrelandings($offer);
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->payout); $i++) {
            $array[$i]['id_original'] = 0;
            $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = '';
            $array[$i]['payment'] = number_format($offer->payout[$i]->amount, 2);
            $array[$i]['currency'] = Adcombo::prepareCurrency($offer->payout[$i]->currency);
            $array[$i]['postclick'] = '';
            if (Text::isArray($offer->payments[$i]->countries)) foreach ($offer->payments[$i]->countries as $geo) $array[$i]['geo'][] = mb_strtoupper($geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        if ($offer->countries == 'all') $array = Text::allCountries();
        else $array = preg_split('/,/u', $offer->countries);
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->name, $array)) $array[] = $cat;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCurrency($offer_currency)
    {
        $currency = 'USD';
        switch ($offer_currency) {
            case '€': {
                $currency = 'EUR';
                break;
            }
        }
        return $currency;
    }

    public static function preparePrelandings($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->prelandings); $i++) {
            $array[$i]['name'] = $offer->prelandings[$i]->name;
            $array[$i]['url'] = $offer->prelandings[$i]->url;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->landing_urls); $i++) $array[$i]['url'] = $offer->landing_urls[$i];
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

}