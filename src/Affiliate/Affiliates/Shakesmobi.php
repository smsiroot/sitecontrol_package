<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Shakesmobi
{
    public static function offersLoad($affiliate)
    {
        $xml = simplexml_load_string(file_get_contents('http://shakes.mobi/export/offervault/offer/list'));
        if (Text::isArray($xml->offers->offer)) foreach ($xml->offers->offer as $offer) {
            $offers[] = Shakesmobi::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->offer_name);
        $offer_new['url'] = mb_strtolower($offer->landings->landing->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = (string)$offer->offer_description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = md5($offer->offer_name . $offer->landings->landing->url);
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 1;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = json_encode([(string)$offer->category], JSON_UNESCAPED_UNICODE);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = mb_strtoupper($offer->currency, 'utf-8');
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Shakesmobi::prepareActions($offer);
        $offer_new['geo'] = Shakesmobi::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->targets->target); $i++) {
            $array[$i]['id_original'] = 0;
            $array[$i]['name'] = Text::ucfirst($offer->targets->target[$i]->kind);
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = '';
            $array[$i]['payment'] = (string)$offer->targets->target[$i]->payout;
            if (is_numeric($array[$i]['payment'])) $array[$i]['payment'] = number_format($array[$i]['payment'], 2);
            $array[$i]['currency'] = mb_strtoupper($offer->currency, 'utf-8');
            $array[$i]['postclick'] = '';
            if (Text::isArray($offer->targets->target[$i]->countries->country)) foreach ($offer->targets->target[$i]->countries->country as $geo) if (!in_array(mb_strtoupper($geo->entry, 'utf-8'), $array)) $array[$i]['geo'][] = mb_strtoupper($geo->entry, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->targets->target); $i++) {
            if (Text::isArray($offer->targets->target[$i]->countries->country)) foreach ($offer->targets->target[$i]->countries->country as $geo) if (!in_array(mb_strtoupper($geo->entry, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->entry, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}