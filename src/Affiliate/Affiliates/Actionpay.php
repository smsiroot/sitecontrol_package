<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Actionpay
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://api.actionpay.net/ru-ru/apiWmOffers/?key=' . $affiliate->apikey . '&format=json&page=1'));
        for ($p = 1; $p <= $json->result->pageCount; $p++) {
            $json = json_decode(file_get_contents('https://api.actionpay.net/ru-ru/apiWmOffers/?key=' . $affiliate->apikey . '&format=json&page=' . $p));
            if (Text::isArray($json->result->offers)) foreach ($json->result->offers as $offer) {
                $offers[] = Actionpay::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->link, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        if ($offer->deeplink) $offer_new['is_deeplink'] = 1;
        else $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        if ($offer->status->id == 2) $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = Actionpay::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        if (isset($offer->aims['0'])) $offer_new['hold'] = $offer->aims['0']->hold;
        if (isset($offer->aims['0'])) $offer_new['postclick'] = $offer->aims['0']->postclick;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Actionpay::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Actionpay::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Actionpay::prepareActions($offer);
        $offer_new['geo'] = Actionpay::prepareGeo(json_decode($offer_new['actions']), $offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        if (isset($offer->aims['0'])) $offer_new['cr'] = $offer->aims['0']->acceptRate;
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::parse($offer->createDate)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->aims); $i++) {
            $array[$i]['id_original'] = $offer->aims[$i]->id;
            $array[$i]['name'] = $offer->aims[$i]->name;
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = $offer->aims[$i]->hold;
            $array[$i]['payment'] = trim(preg_replace('/( [a-zA-Z]+)$/iu', '', $offer->aims[$i]->price));
            $array[$i]['currency'] = mb_strtoupper(Text::match('/ ([A-Za-z]+)$/iu', $offer->aims[$i]->price), 'utf-8');
            $array[$i]['postclick'] = $offer->aims[$i]->postclick;
            if (Text::isArray($offer->aims[$i]->geo->includeCountries)) foreach ($offer->aims[$i]->geo->includeCountries as $geo) $array[$i]['geo'][] = mb_strtoupper($geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($actions, $offer)
    {
        $array = [];
        for ($i = 0; $i < count($actions); $i++) {
            if (Text::isArray($actions[$i]->geo)) foreach ($actions[$i]->geo as $geo) if (!in_array(mb_strtoupper($geo, 'utf-8'), $array)) $array[] = mb_strtoupper($geo, 'utf-8');
        }
        if ($offer->geo->includeCountries === true && count($array) == 0) $array = Text::allCountries();
        if (Text::isArray($offer->geo->includeCountries) && count($array) == 0) foreach ($offer->geo->includeCountries as $geo) if (!in_array(mb_strtoupper($geo, 'utf-8'), $array)) $array[] = mb_strtoupper($geo, 'utf-8');
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->name, $array)) $array[] = $cat->name;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->trafficTypes)) foreach ($offer->trafficTypes as $rule) {
            if ($rule->status != 1) {
                $array[$i]['id_original'] = '';
                $array[$i]['name'] = $rule->name;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->trafficTypes)) foreach ($offer->trafficTypes as $rule) {
            if ($rule->status == 1) {
                $array[$i]['id_original'] = '';
                $array[$i]['name'] = $rule->name;
                $array[$i]['is_allowed'] = 0;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function getAffiliateLink($affiliate, $project_id, $offer_id, $getArray = false)
    {
        $array = [];
        $json = json_decode(file_get_contents('https://api.actionpay.net/ru-ru/apiWmLinks/?key=' . $affiliate->apikey . '&format=json&offer=' . $offer_id . '&source=' . $project_id));
        if (Text::isArray($json->result->links)) foreach ($json->result->links as $link) {
            $array[] = $link->url;
        }
        if ($getArray) return $array;
        else return $array['0'];
    }
}