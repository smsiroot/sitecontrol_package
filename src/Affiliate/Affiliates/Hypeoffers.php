<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Hypeoffers
{
    public static function offersLoad($affiliate)
    {
        $data = json_decode(file_get_contents('https://hypeoffers.com/api/v1/partner/get-offers?key=' . $affiliate->apikey));
        if (Text::isArray($data)) foreach ($data as $offer) {
            $offers[] = Hypeoffers::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Hypeoffers::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Hypeoffers::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Hypeoffers::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Hypeoffers::prepareActions($offer);
        $offer_new['geo'] = Hypeoffers::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Hypeoffers::prepareLandings($offer);
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals); $i++) {
            $array[$i]['id_original'] = $offer->goals[$i]->id;
            $array[$i]['name'] = Text::ucfirst($offer->goals[$i]->title);
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = $offer->hold;
            if (is_numeric($offer->goals[$i]->profit)) $array[$i]['payment'] = number_format($offer->goals[$i]->profit, 2);
            else $array[$i]['payment'] = $offer->goals[$i]->profit;
            $array[$i]['currency'] = mb_strtoupper($offer->goals[$i]->currency_price, 'utf-8');
            $array[$i]['postclick'] = '';
            if (Text::isArray($offer->goals[$i]->geo)) foreach ($offer->goals[$i]->geo as $geo) $array[$i]['geo'][] = $geo->code;
            if ($offer->goals[$i]->isPercent == true) $array[$i]['payment'] = $array[$i]['payment'] . '%';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (Text::isArray($offer->geo)) foreach ($offer->geo as $geo) if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->code, 'utf-8');
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->title, $array)) $array[] = $cat->title;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 1) {
                $array[$i]['id_original'] = '';
                $array[$i]['name'] = $rule->title;
                $i++;
            }
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 0) {
                $array[$i]['id_original'] = '';
                $array[$i]['name'] = $rule->title;
                $i++;
            }
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->landings); $i++) {
            $array[$i]['name'] = $offer->landings[$i]->title;
            $array[$i]['url'] = $offer->landings[$i]->url;
            $array[$i]['type'] = '';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}