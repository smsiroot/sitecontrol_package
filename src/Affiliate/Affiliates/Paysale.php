<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Paysale
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('http://admin.paysale.com/api/v2/offers'));
        for ($p = 1; $p <= ceil($json->pagination->total_count / $json->pagination->per_page); $p++) {
            $json = json_decode(file_get_contents('http://admin.paysale.com/api/v2/offers?locale=' . $affiliate->api_id));
            if (Text::isArray($json->offers)) foreach ($json->offers as $offer) {
                $offers[] = Paysale::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = '';
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = [$offer->category];
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->cookie;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Paysale::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Paysale::prepareActions($offer);
        $offer_new['geo'] = Paysale::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::parse($offer->activate_date)->toDateTimeString();
        dd($offer_new);
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->tariffs); $i++) {
            $array[$i]['id_original'] = 0;
            $array[$i]['name'] = $offer->tariffs[$i]->goal;
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = $offer->hold;
            $array[$i]['payment'] = number_format($offer->tariffs[$i]->payout, 2);
            $array[$i]['currency'] = Paysale::prepareCurrency($offer->tariffs[$i]->currency);
            $array[$i]['postclick'] = $offer->cookie;
            $array[$i]['geo'][] = mb_strtoupper($offer->tariffs[$i]->country_short, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->tariffs); $i++) {
            $array[] = $offer->tariffs[$i]->country_short;
        }
        if (count($array) == 0) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            $array[$i]['id_original'] = '';
            $array[$i]['name'] = $rule->name;
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCurrency($offer)
    {
        $currency = 'USD';
        switch ($offer->currency) {
            case '€': {
                $currency = 'EUR';
                break;
            }
        }
        return $currency;
    }
}