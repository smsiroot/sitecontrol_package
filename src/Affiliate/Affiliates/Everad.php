<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Sitecontrol\Text;

class Everad
{
    public static function authorize($login, $password)
    {
        $client = new Client([
            'allow_redirects' => true,
            'timeout' => 60.0,
            'cookies' => true
        ]);
        $client->request('put', 'https://dashboard.everad.com/v2/session', [
            \GuzzleHttp\RequestOptions::JSON => ['email' => $login, 'password' => $password]
        ]);
        return $client;
    }

    public static function offersLoad($affiliate)
    {
        $client = Everad::authorize($affiliate->api_id, $affiliate->apikey);
        $offers_original = json_decode($client->get('https://dashboard.everad.com/v2/offers')->getBody()->getContents());
        foreach ($offers_original as $offer) {
            $offers[] = Everad::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->domain, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = $offer->rules;
        $offer_new['image'] = $offer->image;
        $offer_new['search_keywords'] = $offer->alternative_titles;
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = ($offer->is_exclusive) ? 1 : 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 1;
        $offer_new['offer_status'] = $offer->status;
        $offer_new['categories_original'] = json_encode([Text::ucfirst($offer->category)], JSON_UNESCAPED_UNICODE);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = 0;
        $offer_new['postclick'] = 180;
        $offer_new['approve_rate'] = $offer->approve;
        $offer_new['landing_price'] = 0;
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Everad::prepareActions($offer);
        $offer_new['geo'] = Everad::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Everad::prepareLandings($offer);
        $offer_new['prelandings'] = Everad::preparePrelandings($offer);
        $offer_new['product_photos'] = Everad::prepareProductPhotos($offer);
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = 0;
        $offer_new['start_time'] = Carbon::parse($offer->created_at)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        if (is_countable($offer->payouts)) for ($i = 0; $i < count($offer->payouts); $i++) {
            $actions[$i]['id_original'] = 0;
            $actions[$i]['name'] = 'Подтвержденная заявка';
            $actions[$i]['hold'] = 0;
            $actions[$i]['payment'] = $offer->payouts[$i]->amount;
            $actions[$i]['currency'] = mb_strtoupper($offer->payouts[$i]->currency, 'utf-8');
            $actions[$i]['postclick'] = 180;
            $actions[$i]['geo'] = [mb_strtoupper($offer->payouts[$i]->country_code, 'utf-8')];
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        if (is_countable($offer->domains)) for ($i = 0; $i < count($offer->domains); $i++) {
            if ($offer->domains[$i]->type == 'landing') {
                $landings[$i]['name'] = $offer->domains[$i]->title;
                $landings[$i]['url'] = $offer->domains[$i]->domain;
                $landings[$i]['type'] = $offer->domains[$i]->type;
            }
        }
        return json_encode($landings, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePrelandings($offer)
    {
        if (is_countable($offer->domains)) for ($i = 0; $i < count($offer->domains); $i++) {
            if ($offer->domains[$i]->type == 'transit') {
                $prelandings[$i]['name'] = $offer->domains[$i]->title;
                $prelandings[$i]['url'] = $offer->domains[$i]->domain;
                $prelandings[$i]['type'] = $offer->domains[$i]->type;
            }
        }
        return json_encode($prelandings, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareProductPhotos($offer)
    {
        if (is_countable($offer->promo->product)) for ($i = 0; $i < count($offer->promo->product); $i++) {
            $photos[$i]['url'] = $offer->promo->product[$i];
            $photos[$i]['name'] = '';
        }
        return json_encode($photos, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (is_countable($offer->payouts)) for ($i = 0; $i < count($offer->payouts); $i++) {
            if (!in_array($offer->payouts[$i]->country_code, $array)) $array[] = mb_strtoupper($offer->payouts[$i]->country_code, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}