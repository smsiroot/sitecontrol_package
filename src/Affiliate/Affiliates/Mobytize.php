<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Mobytize
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://api.mobytize.com/v1/offer/getAll.json'));
        if (Text::isArray($json->data)) foreach ($json->data as $offer) {
            $offers[] = Mobytize::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->preview_url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->icon_url;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Mobytize::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Mobytize::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Mobytize::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Mobytize::prepareActions($offer);
        $offer_new['geo'] = Mobytize::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::parse($offer->published_at);
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        if (count($offer->goals) < 15)
        {
            for ($i = 0; $i < count($offer->goals); $i++) {
                $array[$i]['id_original'] = $offer->goals[$i]->id;
                $array[$i]['name'] = $offer->goals[$i]->name;
                $array[$i]['hold'] = $offer->hold;
                $array[$i]['payment'] = number_format($offer->goals[$i]->payout, 2);
                if ($offer->goals[$i]->is_percent == true) $array[$i]['payment'] = $array[$i]['payment'] . '%';
                $array[$i]['currency'] = mb_strtoupper($offer->goals[$i]->currency, 'utf-8');
                $array[$i]['postclick'] = $offer->postclick;
                $array[$i]['geo'][] = mb_strtoupper($offer->goals[$i]->country_code, 'utf-8');
            }
        }
        else {
            $array['0']['id_original'] = $offer->goals['0']->id;
            $array['0']['name'] = $offer->goals['0']->name;
            $array['0']['hold'] = $offer->hold;
            $array['0']['payment'] = number_format($offer->goals['0']->payout, 2);
            if ($offer->goals['0']->is_percent == true) $array['0']['payment'] = $array['0']['payment'] . '%';
            $array['0']['currency'] = mb_strtoupper($offer->goals['0']->currency, 'utf-8');
            $array['0']['postclick'] = $offer->postclick;
            $array['0']['geo'] = Mobytize::prepareGeo($offer);
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (Text::isArray($offer->geo)) foreach ($offer->geo as $geo) if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->code, 'utf-8');
        if (count($array) == 0) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->vertical)) foreach ($offer->vertical as $cat) if (!in_array($cat->name, $array) && $cat->name != '') $array[] = $cat->name;
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->name, $array) && $cat->name != '') $array[] = $cat->name;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }

        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal != 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}