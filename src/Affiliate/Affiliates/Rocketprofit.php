<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Sitecontrol\Text;

class Rocketprofit
{
    public static function offersLoad($affiliate)
    {
        $client = new Client([
            'allow_redirects' => true,
            'timeout' => 60.0,
            'cookies' => true
        ]);

        $json = json_decode($client->get('https://rocketprofit.com/api/public-offer-list/actualtraffic?lang=ru')->getBody()->getContents());
        if (Text::isArray($json)) foreach ($json as $offer) {
            $offers[] = Rocketprofit::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->domain, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = trim($offer->description);
        $offer_new['text_rules'] = trim($offer->rules);
        $offer_new['image'] = $offer->image;
        $offer_new['search_keywords'] = Rocketprofit::prepareSearchKeywords($offer);
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 1;
        $offer_new['offer_status'] = $offer->status;
        $offer_new['categories_original'] = json_encode([$offer->category], JSON_UNESCAPED_UNICODE);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = $offer->approve;
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Rocketprofit::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Rocketprofit::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Rocketprofit::prepareActions($offer);
        $offer_new['geo'] = Rocketprofit::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Rocketprofit::prepareLandings($offer);
        $offer_new['prelandings'] = Rocketprofit::preparePrelandings($offer);
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = 0;
        $offer_new['start_time'] = Carbon::parse($offer->created_at)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->payouts); $i++) {
            $actions[$i]['id_original'] = 0;
            $actions[$i]['name'] = 'Подтвержденная заявка';
            $actions[$i]['hold'] = $offer->hold;
            $actions[$i]['payment'] = $offer->payouts[$i]->amount;
            $actions[$i]['currency'] = mb_strtoupper($offer->payouts[$i]->currency, 'utf-8');
            $actions[$i]['postclick'] = $offer->postclick;
            $actions[$i]['geo'] = [mb_strtoupper($offer->payouts[$i]->country_code, 'utf-8')];
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        for ($i = 0; $i < count($offer->landings); $i++) {
            $landings[$i]['name'] = $offer->domains[$i]->title;
            $landings[$i]['url'] = $offer->domains[$i]->domain;
            $landings[$i]['type'] = 'landing';
        }
        return json_encode($landings, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePrelandings($offer)
    {
        for ($i = 0; $i < count($offer->transits); $i++) {
            $prelandings[$i]['name'] = $offer->domains[$i]->title;
            $prelandings[$i]['url'] = $offer->domains[$i]->domain;
            $prelandings[$i]['type'] = 'transit';
        }
        return json_encode($prelandings, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 0) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $geo = [];
        for ($i = 0; $i < count($offer->payouts); $i++) {
            if (!in_array($offer->payouts[$i]->country_code, $geo)) $geo[] = $offer->payouts[$i]->country_code;
        }
        return json_encode($geo, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareSearchKeywords($offer)
    {
        $keywords = '';
        if (Text::isArray($offer->keywords)) foreach ($offer->keywords as $string) $keywords = $keywords . ' ' . $string;
        return trim($keywords);
    }
}