<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Expertmobi
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://expertmobi.com/api/offers'));
        if (Text::isArray($json)) foreach ($json as $offer) {
            $offers[] = Expertmobi::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->offer_name);
        $offer_new['url'] = mb_strtolower($offer->link, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->rules;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Expertmobi::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = Expertmobi::prepareCurrency($offer);
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Expertmobi::prepareActions($offer);
        $offer_new['geo'] = Expertmobi::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::parse($offer->date_start)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        $array['0']['id_original'] = '';
        $array['0']['name'] = 'Conversion';
        $array['0']['hold'] = '0';
        if (is_numeric($offer->rate)) $array['0']['payment'] = number_format($offer->rate, 2);
        else $array['0']['payment'] = $offer->rate;
        $array['0']['currency'] = Expertmobi::prepareCurrency($offer);
        $array['0']['postclick'] = 0;
        $array['0']['geo'] = '';
        if ($offer->is_percent == true) $array['0']['payment'] = $array['0']['payment'] . '%';
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (Text::isArray($offer->geo)) foreach ($offer->geo as $geo) if (!in_array(mb_strtoupper($geo, 'utf-8'), $array)) $array[] = mb_strtoupper($geo, 'utf-8');
        if ($offer->geo == null) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->category)) foreach ($offer->category as $cat) if (!in_array($cat, $array)) $array[] = $cat;
        if (Text::isArray($offer->type)) foreach ($offer->type as $cat) if (!in_array($cat, $array)) $array[] = $cat;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCurrency($offer)
    {
        $currency = 'USD';
        switch ($offer->currency) {
            case '€': {
                $currency = 'EUR';
                break;
            }
        }
        return $currency;
    }
}