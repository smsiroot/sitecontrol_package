<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Affise
{
    public static function offersLoad($affiliate)
    {
        if ($affiliate->apikey != '') $apikey = '?API-Key=' . $affiliate->apikey;
        $json = json_decode(file_get_contents('http://' . $affiliate->api_id . '/3.0/offers' . $apikey));
        for ($p = 1; $p <= ceil($json->pagination->total_count / $json->pagination->per_page); $p++) {
            if ($affiliate->apikey != '') $getParams = $apikey . '&page=' . $p;
            $json = json_decode(file_get_contents('http://' . $affiliate->api_id . '/3.0/offers' . $getParams));
            if (Text::isArray($json->offers)) foreach ($json->offers as $offer) {
                $offers[] = Affise::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->preview_url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        if ($offer->required_approval) $offer_new['is_moderation'] = 1;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Affise::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold_period;
        $offer_new['postclick'] = Affise::preparePostclick($offer);
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Affise::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Affise::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Affise::prepareActions($offer);
        $offer_new['geo'] = Affise::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = $offer->cr;
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = $offer->epc;
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->payments); $i++) {
            $array[$i]['id_original'] = 0;
            $array[$i]['name'] = $offer->payments[$i]->title;
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = $offer->hold_period;
            $array[$i]['payment'] = number_format($offer->payments[$i]->revenue, 2);
            $array[$i]['currency'] = mb_strtoupper($offer->payments[$i]->currency, 'utf-8');
            $array[$i]['postclick'] = Affise::preparePostclick($offer);
            if (Text::isArray($offer->payments[$i]->countries)) foreach ($offer->payments[$i]->countries as $geo) $array[$i]['geo'][] = mb_strtoupper($geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->payments); $i++) {
            if (Text::isArray($offer->payments[$i]->countries)) foreach ($offer->payments[$i]->countries as $geo) if (!in_array(mb_strtoupper($geo, 'utf-8'), $array)) $array[] = mb_strtoupper($geo, 'utf-8');
        }
        if (count($array) == 0) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->name, $array)) $array[] = $cat;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->allowed == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->allowed != 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePostclick($offer)
    {
        if (preg_match('/d/iu', $offer->click_session)) $postclick = Text::match('/([0-9]+)/iu', $offer->click_session);
        if (preg_match('/m/iu', $offer->click_session)) $postclick = (Text::match('/([0-9]+)/iu', $offer->click_session) * 30);
        if (preg_match('/y/iu', $offer->click_session)) $postclick = (Text::match('/([0-9]+)/iu', $offer->click_session) * 364);
        return $postclick;
    }
}