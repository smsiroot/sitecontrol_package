<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Ad1
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('http://api.ad1.ru/v1/WmOffers/?key=' . $affiliate->apikey));
        if (Text::isArray($json->data)) foreach ($json->data as $offer) {
            $offers[] = Ad1::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->fullDescription;
        $offer_new['text_rules'] = $offer->rules;
        $offer_new['image'] = $offer->screen;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Ad1::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = mb_strtoupper($offer->currency, 'utf-8');
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Ad1::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Ad1::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Ad1::prepareActions($offer);
        $offer_new['geo'] = Ad1::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Ad1::prepareLandings($offer);
        $offer_new['prelandings'] = Ad1::preparePrelandings($offer);
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = $offer->cr;
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = $offer->epc;
        $offer_new['start_time'] = Carbon::parse($offer->createdAt)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals); $i++) {
            $array[$i]['id_original'] = $offer->goals[$i]->id;
            $array[$i]['name'] = $offer->goals[$i]->title;
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = $offer->hold;
            if (is_numeric($offer->goals[$i]->profit)) $array[$i]['payment'] = number_format($offer->goals[$i]->profit, 2);
            else $array[$i]['payment'] = $offer->goals[$i]->profit;
            $array[$i]['currency'] = mb_strtoupper($offer->goals[$i]->currency, 'utf-8');
            $array[$i]['postclick'] = $offer->postclick;
            $array[$i]['geo'] = '';
            if ($offer->goals[$i]->isPercent == true) $array[$i]['payment'] = $array[$i]['payment'] . '%';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (Text::isArray($offer->geo->countrys)) foreach ($offer->geo->countrys as $geo) if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->code, 'utf-8');
        if ($offer->geo == null) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->cats)) foreach ($offer->cats as $cat) if (!in_array($cat->title, $array)) $array[] = $cat->title;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal != 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
                $array[$i]['is_allowed'] = 0;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function getAffiliateLink($affiliate, $project_id, $offer_id, $getArray = false)
    {
        $array = [];
        $json = json_decode(file_get_contents('https://api.Ad1.net/ru-ru/apiWmLinks/?key=' . $affiliate->apikey . '&format=json&offer=' . $offer_id . '&source=' . $project_id));
        if (Text::isArray($json->result->links)) foreach ($json->result->links as $link) {
            $array[] = $link->url;
        }
        if ($getArray) return $array;
        else return $array['0'];
    }

    public static function prepareLandings($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->landings); $i++) {
            $array[$i]['name'] = $offer->landings[$i]->title;
            $array[$i]['url'] = $offer->landings[$i]->url;
            $array[$i]['type'] = '';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePrelandings($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->transits); $i++) {
            $array[$i]['name'] = $offer->transits[$i]->title;
            $array[$i]['url'] = $offer->transits[$i]->url;
            $array[$i]['type'] = '';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}