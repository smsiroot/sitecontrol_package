<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Parser;
use Sitecontrol\Text;

class Cityads
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(Parser::getContent('http://cityads.com/api/rest/webmaster/json/offers/all?remote_auth=' . $affiliate->apikey . '&lang=' . $affiliate->api_id));
        for ($p = 0; $p <= ceil($json->data->total / 200); $p++) {
            $json = json_decode(Parser::getContent('http://cityads.com/api/rest/webmaster/json/offers/all?remote_auth=' . $affiliate->apikey . '&lang=' . $affiliate->api_id . '&start=' . $p . '&limit=200'));
            if (Text::isArray($json->data->items)) foreach ($json->data->items as $offer) {
                $offers[] = Cityads::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_info = Cityads::getOffer($offer->id, $affiliate);
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->site, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer_info->text;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->screen;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer->is_exclusive == '1' ? $offer_new['is_exclusive'] = 1 : $offer->is_exclusive = 0;
        $offer_info->is_deeplink_enabled == true ? $offer_new['is_deeplink'] = 1 : $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Cityads::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = $offer->cookie_ltv;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Cityads::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Cityads::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Cityads::prepareActions($offer, $affiliate);
        $offer_new['geo'] = Cityads::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = $offer->epc90days;
        $offer_new['start_time'] = Carbon::parse($offer_info->date_start)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function getOffer($id, $affiliate)
    {
        $json = json_decode(Parser::getContent('http://cityads.com/api/rest/webmaster/json/offer/' . $id . '?remote_auth=' . $affiliate->apikey . '&lang=' . $affiliate->api_id));
        if (Text::isArray($json->data)) return $json->data;
    }

    public static function prepareActions($offer, $affiliate)
    {
        $array = [];
        $i = 0;
        $json = json_decode(Parser::getContent('http://cityads.com/api/rest/webmaster/json/offertarget/' . $offer->id . '?remote_auth=' . $affiliate->apikey . '&lang=' . $affiliate->api_id));
        if (Text::isArray($json->data->items)) foreach ($json->data->items as $action) {
            $array[$i]['id_original'] = $action->id;
            $array[$i]['name'] = Text::ucfirst($action->title);
            $array[$i]['hold'] = '0';
            if ($action->cpl != null) $array[$i]['payment'] = $action->cpl;
            if ($action->cps != null) $array[$i]['payment'] = $action->cps;
            $array[$i]['payment'] = str_replace('\'', '', $array[$i]['payment']);
            $array[$i]['currency'] = Text::match('/ ([a-zA-Z]+)$/ui', $array[$i]['payment']);
            $array[$i]['postclick'] = $offer->cookie_ltv;
            if (Text::isArray($action->service_area)) foreach ($action->service_area as $geo) if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) {
                if (mb_strtolower($geo->code, 'utf-8') == 'wrld') $array[$i]['geo'] = Text::allCountries();
                else $array[$i]['geo'][] = mb_strtoupper($geo->code, 'utf-8');
            }
            if (isset($array[$i]['geo']['0'])) if (mb_strtoupper($array[$i]['geo']['0'], 'utf-8') == 'SNG') $array[$i]['geo'] = Text::getCIS();
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (Text::isArray($offer->geo)) foreach ($offer->geo as $geo) if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->code, 'utf-8');
        if (count($offer->geo) == 1 && mb_strtolower($offer->geo['0']->code, 'utf-8') == 'wrld') $array = Text::allCountries();
        if (count($offer->geo) == 1 && mb_strtoupper($offer->geo['0']->code, 'utf-8') == 'SNG') $array = Text::getCIS();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (Text::isArray($cat->other_categories)) foreach ($cat->other_categories as $cat2) if (!in_array($cat2->title, $array)) $array[] = $cat2->title;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $el = 'traffic-types';
        $i = 0;
        if (Text::isArray($offer->$el)) foreach ($offer->$el as $rule) {
            if ($rule->value == true) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->name;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $el = 'traffic-types';
        $i = 0;
        if (Text::isArray($offer->$el)) foreach ($offer->$el as $rule) {
            if ($rule->value != true) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->name;
                $array[$i]['is_allowed'] = 0;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}