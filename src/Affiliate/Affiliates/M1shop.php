<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class M1shop
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://m1-shop.ru/offers_export_api/?webmaster_id='.$affiliate->api_id.'&api_key=' . $affiliate->apikey));
        foreach ($json as $offer) {
            $offers[] = M1shop::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->landing['0']->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->info;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->img;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = '';
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = $offer->cr;
        $offer_new['landing_price'] = $offer->target['0']->price . ' ' . $offer->target['0']->currency;
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = M1shop::prepareActions($offer);
        $offer_new['geo'] = M1shop::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = M1shop::prepareLandings($offer);
        $offer_new['prelandings'] = '';
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = $offer->epc;
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->target); $i++) {
            $actions[$i]['id_original'] = 0;
            $actions[$i]['name'] = 'Подтвержденная заявка';
            $actions[$i]['hold'] = '';
            $actions[$i]['payment'] = Text::match('/([0-9]+)/u', $offer->target[$i]->pay);
            $actions[$i]['currency'] = 'RUB';//$offer->target[$i]->currency;
            $actions[$i]['postclick'] = '';
            $actions[$i]['geo'] = [$offer->target[$i]->code];
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        for ($i = 0; $i < count($offer->landing); $i++) {
            $landings[$i]['name'] = '';
            $landings[$i]['url'] = $offer->landing[$i]->url;
            if ($offer->landing[$i]->type == 'd') $landings[$i]['type'] = 'Десктоп';
            if ($offer->landing[$i]->type == 'm') $landings[$i]['type'] = 'Мобильный лендинг';
        }
        return json_encode($landings, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePrelandings($offer)
    {
        for ($i = 0; $i < count($offer->transit_page); $i++) {
            $prelandings[$i]['url'] = $offer->transit_page[$i]->url;
        }
        return json_encode($prelandings, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $geo = [];
        for ($i = 0; $i < count($offer->target); $i++) {
            if (!in_array(mb_strtoupper($offer->target[$i]->code, 'utf-8'), $geo)) $geo[] = mb_strtoupper($offer->target[$i]->code, 'utf-8');
        }
        return json_encode($geo, JSON_UNESCAPED_UNICODE);
    }
}