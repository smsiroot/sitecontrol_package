<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Mylead
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://mylead.global/api/external/mylead/programs?token=HVKwpcCcJ6BL0OVpKBGLJ22dVjXa6Tc0ISaq2k1fkKslVbtNnalu3nI5OqI1'));
        if (Text::isArray($json->data)) foreach ($json->data as $offer) $offers[] = Mylead::prepareOffer($offer, $affiliate);
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->preview_url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description_ru;
        $offer_new['text_rules'] = $offer->terms_ru;
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 1;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Mylead::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold_period;
        $offer_new['postclick'] = $offer->cookies_days;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Mylead::prepareActions($offer);
        $offer_new['geo'] = Mylead::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        $array['0']['id_original'] = 0;
        $array['0']['name'] = $offer->conversion_ru;
        if ($array['0']['name'] == '') $array['0']['name'] = 'Conversion';
        $array['0']['hold'] = 0;
        $array['0']['payment'] = Text::match('/([0-9,\.]+)/', $offer->max_payout_USD);
        $array['0']['currency'] = 'USD';
        $array['0']['postclick'] = $offer->cookies_days;
        $array['0']['geo'] = preg_split('/,/u', $offer->countries);
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        $array = preg_split('/,/u', $offer->countries);
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        $array = preg_split('/,/u', $offer->category_ru);
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->allowed == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->allowed != 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}