<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Parser;
use Sitecontrol\Text;

class Leadssu
{
    public static function offersLoad($affiliate)
    {
        // Dictionaries
        $affiliate->dictionary = new \stdClass();
        $affiliate->dictionary->categories = json_decode(file_get_contents('http://api.leads.su/webmaster/dictionary/categories?token=' . $affiliate->apikey))->data;
        $affiliate->dictionary->traffics = json_decode(file_get_contents('http://api.leads.su/webmaster/dictionary/traffics?token=' . $affiliate->apikey))->data;
        // Offers
        $i = true;
        $page = 0;
        while ($i) {
            $offset = $page * 50;
            $json = json_decode(file_get_contents('http://api.leads.su/webmaster/offers?offset=' . $offset . '&geo=1&extendedFields=0&limit=50&token=' . $affiliate->apikey));
            if (Text::isArray($json->data)) foreach ($json->data as $offer) {
                $offers[] = Leadssu::prepareOffer($offer, $affiliate);
            }
            $page++;
            if (count($json->data) == 0) $i = false;
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->site_name);
        if ($offer_new['name'] == '') $offer_new['name'] = trim(preg_replace('/\[.*\]/u', '', Text::ucfirst($offer->name)));
        $offer_new['url'] = mb_strtolower($offer->offer_url, 'utf-8');
//        $offer_new['url'] = Parser::getRedirectUrl(mb_strtolower($offer->offer_url, 'utf-8'));
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = Leadssu::prepareCategories($offer, $affiliate);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = 'RUB';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = $offer->session_hours / 24;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Leadssu::prepareTrafficAllowed($offer, $affiliate);
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Leadssu::prepareActions($offer);
        $offer_new['geo'] = Leadssu::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = $offer->detail_stats->system->other_cr;
        $offer_new['ratio'] = $offer->detail_stats->system->other_ctr;
        $offer_new['ecpc'] = $offer->detail_stats->system->other_epc;
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareCategories($offer, $affiliate)
    {
        $array = [];
        foreach ($offer->categories as $cid) {
            foreach ($affiliate->dictionary->categories as $cat) if ($cat->id == $cid) $array[] = $cat->name;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer, $affiliate)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->traffic_types)) foreach ($offer->traffic_types as $id) {
            foreach ($affiliate->dictionary->traffics as $traff) if ($traff->id == $id) {
                $array[$i]['id_original'] = '';
                $array[$i]['name'] = $traff->name;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals); $i++) {
            $array[$i]['id_original'] = $offer->goals[$i]->id;
            $array[$i]['name'] = $offer->goals[$i]->name;
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = 0;
            if (is_numeric($offer->goals[$i]->payout)) $array[$i]['payment'] = number_format($offer->goals[$i]->payout, 2);
            else $array[$i]['payment'] = $offer->goals[$i]->payout;
            $array[$i]['currency'] = 'RUB';
            $array[$i]['postclick'] = $offer->session_hours / 24;
            $array[$i]['geo'] = '';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        if (Text::isArray($offer->geo->countries)) foreach ($offer->geo->countries as $geo) if (!in_array(mb_strtoupper($geo->iso, 'utf-8'), $array)) $array[] = Leadssu::getCodeFromAlpha3(mb_strtoupper($geo->iso, 'utf-8'));
        if ($offer->geo->countries == null) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function getCodeFromAlpha3($alpha3)
    {
        $allCountries = Text::allCountriesIso();
        foreach ($allCountries as $key => $value) if ($value['1'] == $alpha3) return $value['0'];
    }
}