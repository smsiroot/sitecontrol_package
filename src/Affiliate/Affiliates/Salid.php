<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Salid
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://salid.ru/api?auth=34422859226590293049'));
        if (Text::isArray($json)) foreach ($json as $offer) {
            $offers[] = Salid::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->landings['0']->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description_full;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = Salid::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = 0;
        $offer_new['landing_price'] = $offer->landing_price;
        $offer_new['traffic_allowed'] = Salid::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Salid::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Salid::prepareActions($offer);
        $offer_new['geo'] = Salid::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Salid::prepareLandings($offer);
        $offer_new['prelandings'] = '';
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = $offer->epc;
        $offer_new['start_time'] = Carbon::parse($offer->start)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->category)) foreach ($offer->category as $cat) if (!in_array($cat->title, $array)) $array[] = $cat->title;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $traffics) {
            if ($traffics->legal) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->title;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $traffics) {
            if (!$traffics->legal) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->name;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->goal); $i++) {
            $actions[$i]['id_original'] = $offer->goal[$i]->id;
            $actions[$i]['name'] = $offer->goal[$i]->title;
            $actions[$i]['hold'] = $offer->hold;
            $actions[$i]['payment'] = $offer->goal[$i]->profit;
            if ($offer->goal[$i]->profit_type == 1) $actions[$i]['payment'] .= '%';
            $actions[$i]['currency'] = mb_strtoupper($offer->goal[$i]->profit_currency, 'utf-8');
            $actions[$i]['postclick'] = $offer->postclick;
            $actions[$i]['geo'] = Salid::prepareGeo($offer);
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        for ($i = 0; $i < count($offer->landings); $i++) {
            $landings[$i]['name'] = $offer->landings[$i]->title;
            $landings[$i]['url'] = $offer->landings[$i]->url;
            $landings[$i]['type'] = $offer->landings[$i]->type;
        }
        return json_encode($landings, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goal); $i++) {
            foreach ($offer->goal[$i]->geo as $geo) {
                if ($geo->code == 'all') $array = Text::allCountries();
                else if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->code, 'utf-8');
            }
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}