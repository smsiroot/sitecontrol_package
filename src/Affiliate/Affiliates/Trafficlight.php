<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Trafficlight
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('http://cpa.tl/api/cpad'));
        if (Text::isArray($json->data)) foreach ($json->data as $offer) {
            $offers[] = Trafficlight::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->landings['0']->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = '';
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = '';
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Trafficlight::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Trafficlight::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Trafficlight::prepareActions($offer);
        $offer_new['geo'] = Trafficlight::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Trafficlight::prepareLandings($offer);
        $offer_new['prelandings'] = '';
        $offer_new['cr'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->goals); $i++) {
            $actions[$i]['id_original'] = 0;
            $actions[$i]['name'] = Text::ucfirst($offer->goals[$i]->title);
            $actions[$i]['hold'] = 0;
            $actions[$i]['payment'] = $offer->goals[$i]->profit;
            if ($offer->goals[$i]->isPercent) $actions[$i]['payment'] = $actions[$i]['payment'] . '%';
            $actions[$i]['currency'] = mb_strtoupper($offer->goals[$i]->currency, 'utf-8');
            $actions[$i]['postclick'] = 180;
            $actions[$i]['geo'] = [mb_strtoupper($offer->goals[$i]->geo, 'utf-8')];
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = Text::ucfirst($rule->title);
                $i++;
            }
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 0) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = Text::ucfirst($rule->title);
                $i++;
            }
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->landings); $i++) {
            $array[$i]['name'] = Text::ucfirst($offer->landings[$i]->title);
            $array[$i]['url'] = $offer->landings[$i]->url;
            $array[$i]['type'] = '';
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->geo->country); $i++) {
            if (!in_array($offer->geo->country[$i]->code, $array)) $array[] = $offer->geo->country[$i]->code;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}