<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Revenuelab
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://aff.revenuelab.biz/api/v1/account/offers/public-info'));
        if (Text::isArray($json)) foreach ($json as $offer) {
            $offers[] = Revenuelab::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = 'https://aff.revenuelab.biz/#/register/2d27f647-9a50-4836-be6d-f6a413032b33';
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = $offer->restrictions;
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        if ($offer->deeplink) $offer_new['is_deeplink'] = 1;
        else $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        if ($offer->status->id == 2) $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = Revenuelab::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = 'USD';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '30';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Revenuelab::prepareActions($offer);
        $offer_new['geo'] = Revenuelab::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        $array[0]['id_original'] = '';
        $array[0]['name'] = $offer->type;
        if ($array[0]['name'] == '') $array[0]['name'] = 'Conversion';
        $array[0]['hold'] = '';
        $array[0]['payment'] = $offer->payout . $offer->currency;
        $array[0]['currency'] = 'USD';
        $array[0]['postclick'] = 0;
        $array[0]['geo'] = json_decode(Revenuelab::prepareGeo($offer));
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->country); $i++) {
            if (Text::isArray($offer->country)) foreach ($offer->country as $geo) if (!in_array(mb_strtoupper($geo, 'utf-8'), $array)) $array[] = mb_strtoupper($geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->category)) foreach ($offer->category as $cat) if (!in_array($cat, $array)) $array[] = $cat;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}