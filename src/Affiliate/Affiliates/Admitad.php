<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Admitad
{
    public static function authorize($clientId, $clientSecret, $scope)
    {
        $api = new \Admitad\Api\Api();
        $response = $api->authorizeClient($clientId, $clientSecret, $scope);
        $api = new \Admitad\Api\Api($response->getResult()->access_token);
        return $api;
    }

    public static function offersLoad($affiliate)
    {
        // Загрузка офферов Admitad
        $api = Admitad::authorize($affiliate->api_id, $affiliate->apikey, 'advcampaigns');
        $iterator = $api->getIterator('/advcampaigns/', [
            'order_by' => 'id'
        ]);
        foreach ($iterator as $campaign) {
            $offers[] = Admitad::prepareOffer($campaign, $affiliate);
        }
        return $offers;
    }

    public static function offersLoadForProject($affiliate, $project_id)
    {
        // Загрузка офферов для площадки
        $api = Admitad::authorize($affiliate->api_id, $affiliate->apikey, 'advcampaigns_for_website');
        $iterator = $api->getIterator('/advcampaigns/website/' . $project_id . '/', [
            'order_by' => 'id'
        ]);
        foreach ($iterator as $campaign) {
            $offers[] = Admitad::prepareOffer($campaign, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        // Форматирование данных
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->site_url, 'utf-8');
        $offer_new['url_ref'] = $offer->gotolink;
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = $offer->more_rules;
        $offer_new['image'] = $offer->image;
        $offer_new['search_keywords'] = $offer->name_aliases;
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = $offer->exclusive;
        $offer_new['is_deeplink'] = $offer->allow_deeplink;
        $offer_new['is_moderation'] = $offer->moderation;
        $offer_new['offer_status'] = $offer->status;
        $offer_new['categories_original'] = Admitad::prepareCategories($offer);
        $offer_new['goods_export_url'] = $offer->products_xml_link;
        $offer_new['currency'] = $offer->currency;
        $offer_new['hold'] = $offer->avg_hold_time;
        $offer_new['postclick'] = $offer->goto_cookie_lifetime;
        $offer_new['approve_rate'] = $offer->rate_of_approve;
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Admitad::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Admitad::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Admitad::prepareActions($offer);
        $offer_new['geo'] = Admitad::prepareGeo($offer);
        $offer_new['feeds'] = Admitad::prepareFeeds($offer);
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = $offer->ecpc;
        $offer_new['start_time'] = Carbon::parse($offer->activation_date)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        if ($offer->actions != null) for ($i = 0; $i < count($offer->actions); $i++) {
            $actions[$i]['id_original'] = $offer->actions[$i]->id;
            $actions[$i]['name'] = $offer->actions[$i]->name;
            $actions[$i]['hold'] = $offer->actions[$i]->hold_time;
            $actions[$i]['payment'] = $offer->actions[$i]->payment_size;
            $actions[$i]['currency'] = $offer->currency;
            $actions[$i]['postclick'] = '';
            $actions[$i]['geo'] = '';
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        if ($offer->categories != null) for ($i = 0; $i < count($offer->categories); $i++) {
            $categories[] = $offer->categories[$i]->name;
        }
        return json_encode($categories, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $i = 0;
        foreach ($offer->traffics as $traffics) {
            if ($traffics->enabled) {
                $traffic[$i]['id_original'] = $traffics->id;
                $traffic[$i]['name'] = $traffics->name;
                $traffic[$i]['is_allowed'] = 1;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $i = 0;
        foreach ($offer->traffics as $traffics) {
            if (!$traffics->enabled) {
                $traffic[$i]['id_original'] = $traffics->id;
                $traffic[$i]['name'] = $traffics->name;
                $traffic[$i]['is_allowed'] = 0;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        if ($offer->regions != null) for ($i = 0; $i < count($offer->regions); $i++) {
            $geo[] = $offer->regions[$i]->region;
        }
        if ($geo != null) if (count($geo) == 1 && $geo['0'] == '00') $geo = Text::allCountries();
        return json_encode($geo, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareFeeds($offer)
    {
        if ($offer->feeds_info != null) for ($i = 0; $i < count($offer->feeds_info); $i++) {
            $feeds[$i]['updated_at'] = Carbon::parse($offer->feeds_info[$i]->admitad_last_update)->toDateTimeString();
            $feeds[$i]['csv_url'] = $offer->feeds_info[$i]->csv_link;
            $feeds[$i]['xml_url'] = $offer->feeds_info[$i]->xml_link;
            $feeds[$i]['name'] = $offer->feeds_info[$i]->name;
        }
        return json_encode($feeds, JSON_UNESCAPED_UNICODE);
    }

    public static function getDeeplink($affiliate, $project_id, $offer_id, $subid = '', $ulp = '')
    {
        // Генератор диплинк
        $api = Admitad::authorize($affiliate->api_id, $affiliate->apikey, 'deeplink_generator');
        try {
            $data = $api->get('/deeplink/' . $project_id . '/advcampaign/' . $offer_id . '/',
                ['subid' => $subid, 'ulp' => $ulp])->getResult();
        } catch (\Exception $e) {}
        if (isset($data['0'])) return $data['0'];
    }

    public static function addOffer($affiliate, $project_id, $offer_id)
    {
        // Подключение оффера
        $api = Admitad::authorize($affiliate->api_id, $affiliate->apikey, 'manage_advcampaigns');
        try {
            $data = $api->post('/advcampaigns/' . $offer_id . '/attach/' . $project_id . '/')->getResult();
        } catch (\Exception $e) {}
        if (isset($data['success'])) return 1;
        else return 0;
    }

    public static function addAllOffers($affiliate, $project_id)
    {
        // Подключение всех офферов
        $offers = Admitad::offersLoad($affiliate);
        foreach ($offers as $offer) {
            Admitad::addOffer($affiliate, $project_id, $offer['id_original']);
        }
        return $offers;
    }
}