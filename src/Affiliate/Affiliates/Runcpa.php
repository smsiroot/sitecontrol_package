<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Runcpa
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://runcpa.com/upload/actual-traffic.json'));
        foreach ($json as $offer) {
            $offers[] = Runcpa::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->landings['0']->url, 'utf-8');
        $offer_new['url_ref'] = '';
//        $offer_new['url_offer'] = $offer->url;
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = strip_tags($offer->content);
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->logo;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = '';
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = '';
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Runcpa::prepareActions($offer);
        $offer_new['geo'] = Runcpa::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::parse(str_replace('.', '-', $offer->createdAt))->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        $i = 0;
        $array[$i]['id_original'] = $offer->goals->id;
        $array[$i]['name'] = $offer->goals->title;
        if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
        $array[$i]['hold'] = '';
        $array[$i]['payment'] = is_numeric($offer->goals->profit) ? number_format($offer->goals->profit, 2) : $offer->goals->profit;
        if ($offer->goals->isPercent == 1) $array[$i]['payment'] .= '%';
        $array[$i]['currency'] = mb_strtoupper($offer->goals->currency_profit, 'utf-8');
        $array[$i]['postclick'] = '';
        if (Text::isArray($offer->goals->geo)) foreach ($offer->goals->geo as $geo) if (!in_array(mb_strtoupper($geo['0'], 'utf-8'), $array)) $array[$i]['geo'][] = mb_strtoupper($geo['0'], 'utf-8');
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->geo); $i++) {
            if (!in_array(mb_strtoupper($offer->geo[$i]['0'], 'utf-8'), $array)) $array[] = mb_strtoupper($offer->geo[$i]['0'], 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}