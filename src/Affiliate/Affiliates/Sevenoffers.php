<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Sevenoffers
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://7offers.ru/api/offer?show_stopped=0'));
        for ($p = 1; $p <= ceil($json->total_count / $json->per_page); $p++) {
            $json = json_decode(file_get_contents('https://7offers.ru/api/offer?show_stopped=0&api_key=' . $affiliate->apikey . '&page=' . $p));
            if (Text::isArray($json->objects)) foreach ($json->objects as $offer) {
                $offers[] = Sevenoffers::prepareOffer($offer, $affiliate);
            }
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->name);
        $offer_new['url'] = mb_strtolower($offer->url_homepage, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        if (isset($offer->rules->full)) $offer_new['text_rules'] = $offer->rules->full;
        $offer_new['image'] = $offer->image;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = $offer->is_exclusive == true ? 1 : 0;
        $offer_new['is_deeplink'] = $offer->is_deeplink == true ? 1 : 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = $offer->is_stopped == true ? 'stopped' : '';
        $offer_new['categories_original'] = Sevenoffers::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = '';
        $offer_new['postclick'] = $offer->cookie_ttl_days;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Sevenoffers::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = '';
        $offer_new['geo'] = Sevenoffers::prepareGeo($offer);
        $offer_new['actions'] = Sevenoffers::prepareActions($offer, $affiliate);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = '';
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::parse($offer->date_created)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer, $affiliate)
    {
        $array = [];
        $i = 0;
        $json = json_decode(file_get_contents('https://7offers.ru/api/goal/?api_key=' . $affiliate->apikey . '&offer_id=' . $offer->id));
        foreach ($json->objects as $action) {
            $array[$i]['id_original'] = $action->id;
            $array[$i]['name'] = $action->description;
            $array[$i]['hold'] = $action->hold_period;
            $array[$i]['payment'] = is_numeric($action->payment) ? number_format($action->payment, 2) : $action->payment;
            if ($action->money_type == 2) $array[$i]['payment'] = $array[$i]['payment'] . '%';
            $array[$i]['currency'] = Sevenoffers::getCurrency($action->currency_id);
            $array[$i]['postclick'] = $offer->cookie_ttl_days;
            $array[$i]['geo'] = count($offer->geo_codes) == 0 ? Text::allCountries() : $offer->geo_codes;
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->geo_codes); $i++) {
            if (!in_array(mb_strtoupper($offer->geo_codes[$i], 'utf-8'), $array)) $array[] = mb_strtoupper($offer->geo_codes[$i], 'utf-8');
        }
        if (count($offer->geo_codes) == 0) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->name, $array)) $array[] = $cat->name;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->allowed_traffic)) foreach ($offer->allowed_traffic as $rule) {
            $array[$i]['id_original'] = '';
            $array[$i]['name'] = $rule;
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function getCurrency($id)
    {
        $array = [
            '1' => 'RUR',
            '2' => 'EUR',
            '3' => 'USD',
            '4' => 'UAH',
            '5' => 'AUD',
            '6' => 'AZN',
            '7' => 'GBP',
            '8' => 'AMD',
            '9' => 'BYN',
            '10' => 'BGN',
            '11' => 'BRL',
            '12' => 'HUF',
            '13' => 'DKK',
            '14' => 'INR',
            '15' => 'KZT',
            '16' => 'CAD',
            '17' => 'KGS',
            '18' => 'CNY',
            '19' => 'LTL',
            '20' => 'MDL',
            '21' => 'NOK',
            '22' => 'PLN',
            '23' => 'RON',
            '24' => 'XDR',
            '25' => 'SGD',
            '26' => 'TJS',
            '27' => 'TRY',
            '28' => 'TMT',
            '29' => 'UZS',
            '30' => 'CZK',
            '31' => 'SEK',
            '32' => 'CHF',
            '33' => 'ZAR',
            '34' => 'KRW',
            '35' => 'JPY',
        ];
        return $array[$id];
    }
}