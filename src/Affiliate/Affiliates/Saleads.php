<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Saleads
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://my.saleads.pro/api/offers/actualtraffic?key=' . $affiliate->apikey));
        if (Text::isArray($json->data)) foreach ($json->data as $offer) {
            $offers[] = Saleads::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = '';
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        if ($offer->required_approval) $offer_new['is_moderation'] = 1;
        $offer_new['offer_status'] = '';
        $offer_new['categories_original'] = '';
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = '';
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Saleads::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Saleads::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Saleads::prepareActions($offer);
        $offer_new['geo'] = Saleads::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['product_photos'] = '';
        $offer_new['cr'] = $offer->percent_value;
        $offer_new['ratio'] = '';
        $offer_new['ecpc'] = '';
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareActions($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals); $i++) {
            $array[$i]['id_original'] = 0;
            $array[$i]['name'] = Text::ucfirst($offer->goals[$i]->title);
            if ($array[$i]['name'] == '') $array[$i]['name'] = 'Conversion';
            $array[$i]['hold'] = $offer->hold;
            if (is_numeric($offer->goals[$i]->profit)) $array[$i]['payment'] = number_format($offer->goals[$i]->profit, 2);
            else $array[$i]['payment'] = $offer->goals[$i]->profit;
            $array[$i]['currency'] = mb_strtoupper($offer->goals[$i]->currency, 'utf-8');
            $array[$i]['postclick'] = $offer->postclick;
            if (Text::isArray($offer->goals[$i]->geo)) foreach ($offer->goals[$i]->geo as $geo) $array[$i]['geo'][] = mb_strtoupper($geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->geo->country); $i++) {
            if (Text::isArray($offer->geo->country)) foreach ($offer->geo->country as $geo) if (!in_array(mb_strtoupper($geo->code, 'utf-8'), $array)) $array[] = mb_strtoupper($geo->code, 'utf-8');
        }
        if (count($array) == 0) $array = Text::allCountries();
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal == 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $array = [];
        $i = 0;
        if (Text::isArray($offer->sources)) foreach ($offer->sources as $rule) {
            if ($rule->legal != 1) {
                $array[$i]['id_original'] = $rule->id;
                $array[$i]['name'] = $rule->title;
            }
            $i++;
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}