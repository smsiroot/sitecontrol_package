<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Scaleo
{
    public static function offersLoad($affiliate)
    {
        $page = 0;
        $perPage = 10;
        do {
            $page++;
            $json = json_decode(file_get_contents("https://{$affiliate->api_id}/api/v2/affiliate/offers?api-key=$affiliate->apikey&page=$page&perPage=$perPage&sortField=id&sortDirection=asc&onlyFeatured=no&onlyNew=no"));
            if (Text::isArray($json->info->offers)) foreach ($json->info->offers as $offer) {
                $offers[] = Scaleo::prepareOffer($offer, $affiliate);
            }
        }
        while (count($json->info->offers) > 0);
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->links['0']->url, 'utf-8');
        $offer_new['url_ref'] = '';
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = $offer->description;
        $offer_new['text_rules'] = '';
        $offer_new['image'] = 'https://s3.eu-central-1.amazonaws.com/storage.scaleo.io/offers/' . $offer->image;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = Scaleo::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = $offer->currency;
        $offer_new['hold'] = '';
        $offer_new['postclick'] = '';
        $offer_new['approve_rate'] = $offer->ar;
        $offer_new['landing_price'] = '';
        $offer_new['traffic_allowed'] = Scaleo::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = '';
        $offer_new['actions'] = Scaleo::prepareActions($offer);
        $offer_new['geo'] = Scaleo::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = '';
        $offer_new['prelandings'] = '';
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = $offer->aff_epc;
        $offer_new['start_time'] = Carbon::parse($offer->created_at)->toDateTimeString();
        return Text::cleanNull($offer_new);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->title, $array)) $array[] = mb_strtolower($cat->title, 'utf-8');
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $i = 0;
        $traffic_types = json_decode($offer->traffic_types_selected);
        if (Text::isArray($traffic_types)) foreach ($traffic_types as $traffics) {
            $traffic[$i]['id_original'] = $traffics->id;
            $traffic[$i]['name'] = $traffics->title;
            $i++;
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $i = 0;
        if (Text::isArray($offer->traffic_types)) foreach ($offer->traffic_types as $traffics) {
            if (!$traffics->allowed) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->name;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->goals); $i++) {
            for ($j = 0; $j < count($offer->goals[$i]->payoutByCountry); $j++) {
                $actions["$i$j"]['id_original'] = 0;
                $actions["$i$j"]['name'] = $offer->goals[$i]->goal_name;
                $actions["$i$j"]['hold'] = '';
                $actions["$i$j"]['payment'] = $offer->goals[$i]->payoutByCountry[$j]->payout->value;
                $actions["$i$j"]['currency'] = mb_strtoupper($offer->currency, 'utf-8');
                $actions["$i$j"]['postclick'] = '';
                $actions["$i$j"]['geo'] = [mb_strtoupper($offer->goals[$i]->payoutByCountry[$j]->country->code, 'utf-8')];
            }
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals[0]->payoutByCountry); $i++) {
            if (!in_array(mb_strtoupper($offer->goals[0]->payoutByCountry[$i]->country->code, 'utf-8'), $array)) $array[] = mb_strtoupper($offer->goals[0]->payoutByCountry[$i]->country->code, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}