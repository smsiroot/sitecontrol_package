<?php

namespace Sitecontrol\Affiliate;

use Carbon\Carbon;
use Sitecontrol\Text;

class Kmabiz
{
    public static function offersLoad($affiliate)
    {
        $json = json_decode(file_get_contents('https://system.kma.biz/actualtraffic?token=sAbZMs9TX9TuX5Sx'));
        if (Text::isArray($json)) foreach ($json as $offer) {
            $offers[] = Kmabiz::prepareOffer($offer, $affiliate);
        }
        return $offers;
    }

    public static function prepareOffer($offer, $affiliate)
    {
        $offer_new['name'] = Text::ucfirst($offer->title);
        $offer_new['url'] = mb_strtolower($offer->landings['0']->url, 'utf-8');
        $offer_new['url_ref'] = $offer->gotolink;
        $offer_new['affiliate_id'] = $affiliate->id;
        $offer_new['category_id'] = 0;
        $offer_new['text_original'] = '';
        $offer_new['text_rules'] = '';
        $offer_new['image'] = $offer->image;
        $offer_new['search_keywords'] = '';
        $offer_new['id_original'] = $offer->id;
        $offer_new['is_exclusive'] = 0;
        $offer_new['is_deeplink'] = 0;
        $offer_new['is_moderation'] = 0;
        $offer_new['offer_status'] = 'active';
        $offer_new['categories_original'] = Kmabiz::prepareCategories($offer);
        $offer_new['goods_export_url'] = '';
        $offer_new['currency'] = '';
        $offer_new['hold'] = $offer->hold;
        $offer_new['postclick'] = $offer->postclick;
        $offer_new['approve_rate'] = 0;
        $offer_new['landing_price'] = $offer->landing_price;
        $offer_new['traffic_allowed'] = Kmabiz::prepareTrafficAllowed($offer);
        $offer_new['traffic_forbidden'] = Kmabiz::prepareTrafficForbidden($offer);
        $offer_new['actions'] = Kmabiz::prepareActions($offer);
        $offer_new['geo'] = Kmabiz::prepareGeo($offer);
        $offer_new['feeds'] = '';
        $offer_new['landings'] = Kmabiz::prepareLandings($offer);
        $offer_new['prelandings'] = Kmabiz::preparePrelandings($offer);
        $offer_new['cr'] = $offer->cr;
        $offer_new['ecpc'] = $offer->ecpc;
        $offer_new['start_time'] = Carbon::now();
        return Text::cleanNull($offer_new);
    }

    public static function prepareCategories($offer)
    {
        $array = [];
        if (Text::isArray($offer->categories)) foreach ($offer->categories as $cat) if (!in_array($cat->title, $array)) $array[] = $cat->title;
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficAllowed($offer)
    {
        $i = 0;
        if (Text::isArray($offer->traffic_types)) foreach ($offer->traffic_types as $traffics) {
            if ($traffics->allowed) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->name;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareTrafficForbidden($offer)
    {
        $i = 0;
        if (Text::isArray($offer->traffic_types)) foreach ($offer->traffic_types as $traffics) {
            if (!$traffics->allowed) {
                $traffic[$i]['id_original'] = '';
                $traffic[$i]['name'] = $traffics->name;
                $i++;
            }
        }
        return json_encode($traffic, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareActions($offer)
    {
        for ($i = 0; $i < count($offer->goals); $i++) {
            $actions[$i]['id_original'] = 0;
            $actions[$i]['name'] = $offer->goals[$i]->type;
            $actions[$i]['hold'] = $offer->hold;
            $actions[$i]['payment'] = $offer->goals[$i]->cost;
            $actions[$i]['currency'] = $offer->goals[$i]->currency;
            $actions[$i]['postclick'] = $offer->postclick;
            $actions[$i]['geo'] = [mb_strtoupper($offer->goals[$i]->geo, 'utf-8')];
        }
        return json_encode($actions, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareLandings($offer)
    {
        for ($i = 0; $i < count($offer->landings); $i++) {
            $landings[$i]['name'] = $offer->landings[$i]->title;
            $landings[$i]['url'] = $offer->landings[$i]->url;
        }
        return json_encode($landings, JSON_UNESCAPED_UNICODE);
    }

    public static function preparePrelandings($offer)
    {
        for ($i = 0; $i < count($offer->transits); $i++) {
            $prelandings[$i]['name'] = $offer->transits[$i]->title;
            $prelandings[$i]['url'] = $offer->transits[$i]->url;
        }
        return json_encode($prelandings, JSON_UNESCAPED_UNICODE);
    }

    public static function prepareGeo($offer)
    {
        $array = [];
        for ($i = 0; $i < count($offer->goals); $i++) {
            if (!in_array(mb_strtoupper($offer->goals[$i]->geo, 'utf-8'), $array)) $array[] = mb_strtoupper($offer->goals[$i]->geo, 'utf-8');
        }
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}