<?php

namespace Sitecontrol;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Parser
{
    use Domains;

    public static function get($url, $timeout = 30, $useragent = '', $referer = '', $headers = '', $proxy = '', $method = 'get', $options = '')
    {
        if ($headers == '') $headers = [
            'User-Agent' => $useragent,
            'referer' => $referer,
        ];
        try {
            $client = new Client([
                'allow_redirects' => true,
                'verify' => false,
                'timeout' => $timeout,
                'cookies' => true,
                'headers' => $headers,
            ]);
            if ($options != '') {
                if ($proxy != '') $options[] = ['proxy' => $proxy];
                return $client->request($method, $url, $options);
            }
            if ($proxy != '') return $client->request($method, $url, ['proxy' => $proxy]);
            return $client->request($method, $url);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            Log::warning('error', $e->getMessage(), 'Parser', 'get');
        }
    }

    public static function getContent($url, $timeout = 30, $useragent = '', $referer = '', $headers = '', $proxy = '', $method = 'get', $options = '')
    {
        $request = self::get($url, $timeout, $useragent, $referer, $headers, $proxy, $method, $options);
        if ($request != null) return $request->getBody()->getContents();
    }

    public static function getProxy($from = 'file', $format = 'guzzle', $geo = '')
    {
        if ($from == 'random') $proxylist = 'zproxy.lum-superproxy.io:22225:lum-customer-hl_cbccab6e-zone-static:gcku46qadiu4';
        if ($from == 'file') $proxylist = '5.61.43.171:3940:user:control_p
185.4.65.43:3940:user:control_p
5.61.58.247:3940:user:control_p';
        if ($geo == 'ru') $proxylist = '185.4.65.43:3940:user:control_p';
        if ($geo == 'en') $proxylist = '5.61.43.171:3940:user:control_p';
        $proxylist = preg_split('/\n/', $proxylist);
        $proxy = $proxylist[rand(0, count($proxylist) - 1)];
        $proxy = preg_split('/:/', $proxy);
        switch ($format) {
            case 'guzzle': {
                return $proxy['2'] . ':' . $proxy['3'] . '@' . trim($proxy['0']) . ':' . $proxy['1'];
                break;
            }
            case 'curl': {
                return ['proxy' => $proxy['0'] . ':' . $proxy['1'], 'auth' => $proxy['2'] . ':' . $proxy['3']];
                break;
            }
        }
    }

    public static function imageCrop($url, $height = 0, $width = 0, $max = 700)
    {
        try {
            $filename = 'storage/temp/' . md5($url) . '.' . pathinfo($url, PATHINFO_EXTENSION);
            $image = \Intervention\Image\ImageManagerStatic::make($url);
            if ($height == 0) {
                $height = $width = ($image->height() >= $image->width()) ? $image->width() : $image->height();
                if ($height > $max) {
                    $height = $width = $max;
                }
            }
            $image->crop($height, $width)->save($filename);
        }
        catch (\Exception $e) {
        }
        return $filename;
    }

    public static function getScreenshotUrl($url, $resolution = '1024x768', $width = 700, $format = 'jpeg')
    {
        $url = Text::prepareUrl($url);
        // Thumbnail
//        return 'https://thumbnail.ws/get/thumbnail/?apikey=ab320428906ef0109167d98e2e2fa6a09fc04203d685&url=' . $url . '&width=700';
        // ScreenshotLayer
//        return 'http://api.screenshotlayer.com/api/capture?access_key=82be10955d8f85f1e06599f4da8a5c28&url=' . $url . '&viewport=' . $resolution . '&width=' . $width . '&format=jpg';
        // S-shot
//        return 'http://mini.s-shot.ru/' . $resolution . '/' . $width . '/' . $format . '/?' . $url;
        return 'http://api.s-shot.ru/KEYOFCS0TOQ566R4TCX/'.$resolution.'/'.$width.'/'.$format.'/?'.$url;
    }

    public static function getFavicon($url)
    {
        return 'https://www.google.com/s2/favicons?domain=' . Text::getDomain($url);
    }

    public static function redirect($url, $type = '301')
    {
        switch ($type) {
            case '301': {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: ' . $url);
                break;
            }
            case 'meta': {
                header('Refresh: 0;url=' . $url);
                break;
            }
        }
        exit;
    }

    public static function getRedirectUrl($url)
    {
        $original_url = $url;
        $continue = true;
        while ($continue) {
            $url = trim($url);
            if ((substr(strtolower($url), 0, 7) != 'http://') && (substr(strtolower($url), 0, 8) != 'https://')) {
                return $original_url;
            }
            $headers = @get_headers($url);
            if (!$headers) {
                return $original_url;
            }
            $new_url = false;
            foreach ($headers as $h) {
                if (substr($h, 0, 10) == 'Location: ') {
                    $new_url = trim(substr($h, 10));
                    break;
                }
            }
            if ($new_url) {
                $url = $new_url;
                $continue = true;
            } else {
                $continue = false;
            }
        }
        if ($url) {
            return $url;
        } else {
            return $original_url;
        }
    }
}