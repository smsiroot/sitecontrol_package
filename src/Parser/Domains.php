<?php

namespace Sitecontrol;

use Illuminate\Support\Str;

trait Domains
{
    public static function getWhois($url)
    {
        $domain = Text::getDomain($url);
        // API whoxy.com
        $whois = json_decode(self::getContent('http://api.whoxy.com/?key=7e5090a366af08br8ad04b2fa876a7ea&whois='.$domain, 60));
        if ($whois->status) {
            $data['domain_registered'] = ($whois->domain_registered == 'yes') ? 1 : 0;
            $data['create_date'] = (isset($whois->create_date)) ? $whois->create_date : '';
            $data['update_date'] = (isset($whois->update_date)) ? $whois->update_date : '';
            $data['expiry_date'] = (isset($whois->expiry_date)) ? $whois->expiry_date : '';
            $data['full_name'] = (isset($whois->registrant_contact->full_name)) ? $whois->registrant_contact->full_name : '';
            $data['company_name'] = (isset($whois->registrant_contact->company_name)) ? $whois->registrant_contact->company_name : '';
            $data['company_address'] = (isset($whois->registrant_contact->mailing_address)) ? $whois->registrant_contact->mailing_address : '';
            $data['city_name'] = (isset($whois->registrant_contact->city_name)) ? $whois->registrant_contact->city_name : '';
            $data['zip_code'] = (isset($whois->registrant_contact->zip_code)) ? $whois->registrant_contact->zip_code : '';
            if (!ctype_digit($data['zip_code'])) {
                $data['zip_code'] = '';
            }
            $data['country_name'] = (isset($whois->registrant_contact->country_name)) ? $whois->registrant_contact->country_name : '';
            $data['country_code'] = (isset($whois->registrant_contact->country_code)) ? $whois->registrant_contact->country_code : '';
            $data['email'] = (isset($whois->registrant_contact->email_address)) ? $whois->registrant_contact->email_address : '';
            $data['phone'] = (isset($whois->registrant_contact->phone_number)) ? $whois->registrant_contact->phone_number : '';
            return $data;
        }
    }

    public static function isWhoisProtect($string)
    {
        if (Str::contains(strtolower($string), [
            'privacy',
            'private',
            'protect',
            'person',
            'whois',
            'proxy',
            'foundation',
            'secure',
            'gdpr-masking',
            'anonymizer',
            'abuse'
        ])) return true;
        else return false;
    }
}