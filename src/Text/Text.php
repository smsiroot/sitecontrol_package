<?php

namespace Sitecontrol;

class Text
{

    public static function ucfirst($string, $encoding = 'utf-8')
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    public static function urlTranslit($string)
    {
        $string = (string)$string;
        $string = strip_tags($string);
        $string = str_replace(array('\n', '\r'), ' ', $string);
        $string = preg_replace('/\s+/', ' ', $string);
        $string = trim($string);
        $string = function_exists('mb_strtolower') ? mb_strtolower($string) : strtolower($string);
        $string = strtr($string, array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''
        ));
        $string = preg_replace('/[^0-9a-z-_ ]/i', '', $string);
        $string = str_replace(' ', '-', $string);
        $string = str_replace('---', '-', $string);
        $string = str_replace('--', '-', $string);
        $string = preg_replace('/-$/i', '', $string);
        return $string;
    }

    public static function prepareText($text)
    {
        $text = str_replace('   ', ' ', trim($text));
        $text = str_replace('  ', ' ', trim($text));
        $text = strtr($text, [
            ' ,' => ',',
            ' .' => '.',
            ' ?' => '?',
            ' !' => '!',
        ]);
        $text = preg_replace('/(\,)([a-zA-Zа-яА-Я])/', "$1 $2", $text);
        $text = preg_replace('/<br?.\/?>$/', '', $text);
        return $text;
    }

    public static function clearText($text)
    {
        $text = trim(htmlspecialchars($text));
        return $text;
    }

    public static function date_ru($date, $is_time = false, $is_second = false)
    {
        $date = strtotime($date);
        if ($date == 0) {
            return false;
        }
        $month_arr = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря'
        ];
        $month = $month_arr[date('n', $date)];
        $year = date('Y', $date);
        $day = date('d', $date);
        $time = date('H:i', $date);
        $time_s = date('H:i:s', $date);
        $date = (!$is_time) ? $day . ' ' . $month . ' ' . $year : $day . ' ' . $month . ' ' . $year . ', ' . $time;
        if ($is_second) $date = $day . ' ' . $month . ' ' . $year . ', ' . $time_s;
        return $date;
    }

    public static function getAliasFromUrl($string)
    {
        $string = str_replace('www.', '', $string);
        if (preg_match('/([а-яА-ЯA-Za-z0-9.-]+)\..*$/u', $string, $match)) {
            $string = str_replace('.', '-', $match[1]);
        }
        return $string;
    }

    public static function getDomain($url)
    {
        $url = trim(self::prepareUrl($url));
        if (preg_match('/[а-яА-Я]/', $url)) {
            // Кириллические домены
            $url = str_replace('http:', '', $url);
            $url = str_replace('https:', '', $url);
            $url = str_replace('www.', '', $url);
            $url = Text::match('/([а-яА-Яa-z0-9A-Z.-]+)\/?/u', $url);
        } else {
            $url = isset(parse_url($url)['host']) == '' ? @parse_url($url)['path'] : @parse_url($url)['host'];
        }
        $url = str_replace('www.', '', $url);
        $url = str_replace('/', '', $url);
        return $url;
    }

    public static function getHost($url)
    {
        return implode('.', array_slice(explode('.', parse_url(Text::prepareUrl($url), PHP_URL_HOST)), -2));
    }

    public static function urlClean($url)
    {
        $url = str_replace('https://', '', $url);
        $url = str_replace('http://', '', $url);
        $url = str_replace('www.', '', $url);
        return $url;
    }

    public static function prepareUrl($url)
    {
        $url = trim($url);
        if (preg_match('/https?:\/\//u', $url) == false) {
            $url = 'http://' . $url;
        }
        return $url;
    }

    public static function getRelativeUrl($url)
    {
        return parse_url($url)['path'];
    }

    public static function highlight($string, $search, $start = '<b>', $end = '</b>')
    {
        return preg_replace('/(' . preg_quote($search, '/') . ')/iu', $start . "$1" . $end, $string);
    }

    public static function match($pattern, $subject)
    {
        preg_match($pattern, $subject, $match);
        if (isset($match['1'])) {
            return $match['1'];
        }
    }

    public static function cleanNull($array)
    {
        foreach ($array as $key => $value) if ($array[$key] == null || $array[$key] == 'null') unset($array[$key]);
        return $array;
    }

    public static function isEmailValid($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) return 1;
        else return 0;
    }

    public static function allCountries()
    {
        return ['AF', 'AL', 'DZ', 'AS', 'AD', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AM', 'AW', 'AU', 'AT', 'AZ', 'BS', 'BH', 'BD', 'BB', 'BY', 'BE', 'BZ', 'BJ', 'BM', 'BT', 'BO', 'BA', 'BW', 'BV', 'BR', 'IO', 'VG', 'BN', 'BG', 'BF', 'BI', 'KH', 'CM', 'CA', 'CV', 'KY', 'CF', 'TD', 'CL', 'CN', 'CX', 'CC', 'CO', 'KM', 'CK', 'CR', 'HR', 'CU', 'CY', 'CZ', 'CD', 'DK', 'DJ', 'DM', 'DO', 'TL', 'EC', 'EG', 'SV', 'GQ', 'ER', 'EE', 'ET', 'FK', 'FO', 'FJ', 'FI', 'FR', 'GF', 'PF', 'TF', 'GA', 'GM', 'GE', 'DE', 'GH', 'GI', 'GR', 'GL', 'GD', 'GP', 'GU', 'GT', 'GN', 'GW', 'GY', 'HT', 'HM', 'HN', 'HK', 'HU', 'IS', 'IN', 'ID', 'IR', 'IQ', 'IE', 'IL', 'IT', 'CI', 'JM', 'JP', 'JO', 'KZ', 'KE', 'KI', 'KW', 'KG', 'LA', 'LV', 'LB', 'LS', 'LR', 'LY', 'LI', 'LT', 'LU', 'MO', 'MK', 'MG', 'MW', 'MY', 'MV', 'ML', 'MT', 'MH', 'MQ', 'MR', 'MU', 'YT', 'MX', 'FM', 'MD', 'MC', 'MN', 'MS', 'MA', 'MZ', 'MM', 'NA', 'NR', 'NP', 'NL', 'AN', 'NC', 'NZ', 'NI', 'NE', 'NG', 'NU', 'NF', 'KP', 'MP', 'NO', 'OM', 'PK', 'PW', 'PS', 'PA', 'PG', 'PY', 'PE', 'PH', 'PN', 'PL', 'PT', 'PR', 'QA', 'CG', 'RE', 'RO', 'RU', 'RW', 'SH', 'KN', 'LC', 'PM', 'VC', 'WS', 'SM', 'ST', 'SA', 'SN', 'CS', 'SC', 'SL', 'SG', 'SK', 'SI', 'SB', 'SO', 'ZA', 'GS', 'KR', 'ES', 'LK', 'SD', 'SR', 'SJ', 'SZ', 'SE', 'CH', 'SY', 'TW', 'TJ', 'TZ', 'TH', 'TG', 'TK', 'TO', 'TT', 'TN', 'TR', 'TM', 'TC', 'TV', 'VI', 'UG', 'UA', 'AE', 'AX', 'GB', 'US', 'UM', 'UY', 'UZ', 'VU', 'VA', 'VE', 'VN', 'WF', 'EH', 'YE', 'ZM', 'ZW', 'ME', 'RS', 'BL', 'BQ', 'CW', 'GG', 'IM', 'JE', 'MF', 'SS', 'SX'];
    }

    public static function allCountriesIso()
    {
        return [['AF', 'AFG'], ['AL', 'ALB'], ['DZ', 'DZA'], ['AS', 'ASM'], ['AD', 'AND'], ['AO', 'AGO'], ['AI', 'AIA'], ['AQ', 'ATA'], ['AG', 'ATG'], ['AR', 'ARG'], ['AM', 'ARM'], ['AW', 'ABW'], ['AU', 'AUS'], ['AT', 'AUT'], ['AZ', 'AZE'], ['BS', 'BHS'], ['BH', 'BHR'], ['BD', 'BGD'], ['BB', 'BRB'], ['BY', 'BLR'], ['BE', 'BEL'], ['BZ', 'BLZ'], ['BJ', 'BEN'], ['BM', 'BMU'], ['BT', 'BTN'], ['BO', 'BOL'], ['BA', 'BIH'], ['BW', 'BWA'], ['BV', 'BVT'], ['BR', 'BRA'], ['IO', 'IOT'], ['VG', 'VGB'], ['BN', 'BRN'], ['BG', 'BGR'], ['BF', 'BFA'], ['BI', 'BDI'], ['KH', 'KHM'], ['CM', 'CMR'], ['CA', 'CAN'], ['CV', 'CPV'], ['KY', 'CYM'], ['CF', 'CAF'], ['TD', 'TCD'], ['CL', 'CHL'], ['CN', 'CHN'], ['CX', 'CXR'], ['CC', 'CCK'], ['CO', 'COL'], ['KM', 'COM'], ['CK', 'COK'], ['CR', 'CRI'], ['HR', 'HRV'], ['CU', 'CUB'], ['CY', 'CYP'], ['CZ', 'CZE'], ['CD', 'COD'], ['DK', 'DNK'], ['DJ', 'DJI'], ['DM', 'DMA'], ['DO', 'DOM'], ['TL', 'TLS'], ['EC', 'ECU'], ['EG', 'EGY'], ['SV', 'SLV'], ['GQ', 'GNQ'], ['ER', 'ERI'], ['EE', 'EST'], ['ET', 'ETH'], ['FK', 'FLK'], ['FO', 'FRO'], ['FJ', 'FJI'], ['FI', 'FIN'], ['FR', 'FRA'], ['GF', 'GUF'], ['PF', 'PYF'], ['TF', 'ATF'], ['GA', 'GAB'], ['GM', 'GMB'], ['GE', 'GEO'], ['DE', 'DEU'], ['GH', 'GHA'], ['GI', 'GIB'], ['GR', 'GRC'], ['GL', 'GRL'], ['GD', 'GRD'], ['GP', 'GLP'], ['GU', 'GUM'], ['GT', 'GTM'], ['GN', 'GIN'], ['GW', 'GNB'], ['GY', 'GUY'], ['HT', 'HTI'], ['HM', 'HMD'], ['HN', 'HND'], ['HK', 'HKG'], ['HU', 'HUN'], ['IS', 'ISL'], ['IN', 'IND'], ['ID', 'IDN'], ['IR', 'IRN'], ['IQ', 'IRQ'], ['IE', 'IRL'], ['IL', 'ISR'], ['IT', 'ITA'], ['CI', 'CIV'], ['JM', 'JAM'], ['JP', 'JPN'], ['JO', 'JOR'], ['KZ', 'KAZ'], ['KE', 'KEN'], ['KI', 'KIR'], ['KW', 'KWT'], ['KG', 'KGZ'], ['LA', 'LAO'], ['LV', 'LVA'], ['LB', 'LBN'], ['LS', 'LSO'], ['LR', 'LBR'], ['LY', 'LBY'], ['LI', 'LIE'], ['LT', 'LTU'], ['LU', 'LUX'], ['MO', 'MAC'], ['MK', 'MKD'], ['MG', 'MDG'], ['MW', 'MWI'], ['MY', 'MYS'], ['MV', 'MDV'], ['ML', 'MLI'], ['MT', 'MLT'], ['MH', 'MHL'], ['MQ', 'MTQ'], ['MR', 'MRT'], ['MU', 'MUS'], ['YT', 'MYT'], ['MX', 'MEX'], ['FM', 'FSM'], ['MD', 'MDA'], ['MC', 'MCO'], ['MN', 'MNG'], ['MS', 'MSR'], ['MA', 'MAR'], ['MZ', 'MOZ'], ['MM', 'MMR'], ['NA', 'NAM'], ['NR', 'NRU'], ['NP', 'NPL'], ['NL', 'NLD'], ['AN', 'ANT'], ['NC', 'NCL'], ['NZ', 'NZL'], ['NI', 'NIC'], ['NE', 'NER'], ['NG', 'NGA'], ['NU', 'NIU'], ['NF', 'NFK'], ['KP', 'PRK'], ['MP', 'MNP'], ['NO', 'NOR'], ['OM', 'OMN'], ['PK', 'PAK'], ['PW', 'PLW'], ['PS', 'PSE'], ['PA', 'PAN'], ['PG', 'PNG'], ['PY', 'PRY'], ['PE', 'PER'], ['PH', 'PHL'], ['PN', 'PCN'], ['PL', 'POL'], ['PT', 'PRT'], ['PR', 'PRI'], ['QA', 'QAT'], ['CG', 'COG'], ['RE', 'REU'], ['RO', 'ROU'], ['RU', 'RUS'], ['RW', 'RWA'], ['SH', 'SHN'], ['KN', 'KNA'], ['LC', 'LCA'], ['PM', 'SPM'], ['VC', 'VCT'], ['WS', 'WSM'], ['SM', 'SMR'], ['ST', 'STP'], ['SA', 'SAU'], ['SN', 'SEN'], ['CS', 'SCG'], ['SC', 'SYC'], ['SL', 'SLE'], ['SG', 'SGP'], ['SK', 'SVK'], ['SI', 'SVN'], ['SB', 'SLB'], ['SO', 'SOM'], ['ZA', 'ZAF'], ['GS', 'SGS'], ['KR', 'KOR'], ['ES', 'ESP'], ['LK', 'LKA'], ['SD', 'SDN'], ['SR', 'SUR'], ['SJ', 'SJM'], ['SZ', 'SWZ'], ['SE', 'SWE'], ['CH', 'CHE'], ['SY', 'SYR'], ['TW', 'TWN'], ['TJ', 'TJK'], ['TZ', 'TZA'], ['TH', 'THA'], ['TG', 'TGO'], ['TK', 'TKL'], ['TO', 'TON'], ['TT', 'TTO'], ['TN', 'TUN'], ['TR', 'TUR'], ['TM', 'TKM'], ['TC', 'TCA'], ['TV', 'TUV'], ['VI', 'VIR'], ['UG', 'UGA'], ['UA', 'UKR'], ['AE', 'ARE'], ['AX', 'ALA'], ['GB', 'GBR'], ['US', 'USA'], ['UM', 'UMI'], ['UY', 'URY'], ['UZ', 'UZB'], ['VU', 'VUT'], ['VA', 'VAT'], ['VE', 'VEN'], ['VN', 'VNM'], ['WF', 'WLF'], ['EH', 'ESH'], ['YE', 'YEM'], ['ZM', 'ZMB'], ['ZW', 'ZWE'], ['ME', 'MNE'], ['RS', 'SRB'], ['BL', ''], ['BQ', 'BES'], ['CW', ''], ['GG', ''], ['IM', ''], ['JE', ''], ['MF', ''], ['SS', ''], ['SX', ''], ['UK', 'GBR']];
    }

    public static function getCIS()
    {
        return ['RU', 'UA', 'AZ', 'AM', 'BY', 'KZ', 'KG', 'MD', 'TJ', 'TM', 'UZ'];
    }

    public static function isArray($var)
    {
        if (is_object($var) || is_array($var)) return 1;
        else return 0;
    }

    public static function ucAfterDot($string)
    {
        preg_match_all("/\. ?([а-яА-Яa-zA-Z])/u", $string, $matches);
        foreach ($matches[0] as $match) $string = str_replace($match, mb_strtoupper($match, 'utf-8'), $string);
        return $string;
    }

    public static function getYoutubeId($url)
    {
        return self::match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $url);
    }

    public static function removeEmoji($text)
    {
        $clean_text = '';
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);
        return $clean_text;
    }
}