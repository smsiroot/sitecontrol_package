var jsgrid = {
    reload: function (container) {
        $('#' + container).jsGrid();
    },
    getSettings: function (container) {
        return $('#' + container).data('jsgrid');
    },
    getConnection: function (el) {
        return $(el).closest('.jsgrid-container').attr('data-connection');
    },
    getId: function (el) {
        return (typeof $(el).attr('data-id') == 'undefined') ? $(el).closest('tr').attr('data-id') : $(el).attr('data-id');
    },
    spinOn: function (container) {
        $('#' + container).find('.jsgrid-controls .jsgrid-ajax-spin').show();
    },
    spinOff: function (container) {
        $('#' + container).find('.jsgrid-controls .jsgrid-ajax-spin').hide();
    }
};