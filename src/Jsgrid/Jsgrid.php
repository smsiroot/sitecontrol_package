<?php

namespace Sitecontrol;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Jsgrid extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    public static function store_remote(array $attributes, Model $model, $connection = null)
    {
        $model = new $model($attributes);
        $model->setConnection($connection);
        $model->save();
        return $model;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
    
    public static function addFilter($settings, $column, $value, $type = 'default')
    {
        // Добавить фильтр
        $filter['column'] = $column;
        $filter['value'] = $value;
        $filter['type'] = $type;
        $settings['table']['filters'][] = $filter;
        return $settings;
    }
    
    public function search(Model $model, $settings)
    {
        // Поиск по всем полям
        if ($settings['table']['searchColumn'] == 'all' && $settings['table']['searchValue'] != '') {
            $settings['table']['columns'] = $model->getConnection()->getSchemaBuilder()->getColumnListing($model->getTable());
            $model = $model->where(function ($query) use ($settings) {
                foreach ($settings['table']['columns'] as $column) {
                    $query = $query->orWhere($column, 'like', '%' . $settings['table']['searchValue'] . '%');
                }
            });
        }
        // Фильтры
        if (isset($settings['table']['filters']) && count($settings['table']['filters']) > 0) {
            foreach ($settings['table']['filters'] as $filter) {
                if ($filter['type'] == 'default') {
                    // Поиск из input
                    $model = $model->where($filter['column'], 'like', '%' . $filter['value'] . '%');
                }
                if ($filter['type'] == 'select') {
                    // Поиск по строгому соответствию
                    $model = $model->where($filter['column'], '=', $filter['value']);
                }
            }
        }
        return $model;
    }
    
    public function getModel(Model $model, $fields, $settings)
    {
        // Формирование модели
        $object = new \stdClass();
        $model = self::search($model, $settings);
        $object->total = $model->count();
        $model = $model
            ->select($fields)
            ->orderBy($settings['table']['orderBy'], $settings['table']['order'])
            ->skip(--$settings['table']['page'] * $settings['table']['limit'])
            ->take($settings['table']['limit'])
            ->get();
        $object->rows = $model;
        return $object;
    }
}
