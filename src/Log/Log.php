<?php

namespace Sitecontrol;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $guarded = ['id'];
    protected $appends = [
        'user'
    ];
    
    public function getUserAttribute()
    {
        return \App\User::on($this->getConnectionName())->find($this->attributes['user_id']);
    }
    
    public function getMessageAttribute()
    {
        return htmlspecialchars($this->attributes['message']);
    }
    
    public static function add($type, $level, $message, $source, $source_function, $di = null)
    {
        $log = new Log();
        if ($di == null) {
            list($di) = debug_backtrace();
        } else {
            list($di) = $di;
        }
        if (\Auth::check()) $log->user_id = \Auth::user()->id;
        else $log->user_id = 0;
        $log->type = $type;
        $log->level = $level;
        $log->message = $message;
        $log->source = $source;
        $log->source_function = $source_function;
        $log->source_file = $di['file'];
        $log->source_line = $di['line'];
        $log->ip = !isset($_SERVER['REMOTE_ADDR']) ? '' : $_SERVER['REMOTE_ADDR'];
        $log->useragent = !isset($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT'];
        $log->save();
    }
    
    public static function is_block($type, $filter_time, $filter_count)
    {
        if (self::getCount($type, $_SERVER['REMOTE_ADDR'], $filter_time) >= $filter_count) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function checkOrFail($type, $filter_time, $filter_count, $source, $source_function)
    {
        if (self::is_block($type, $filter_time, $filter_count)) self::abort(429, $source, $source_function);
        return true;
    }
    
    public static function addOrFail($type, $level, $message, $source, $source_function, $filter_time, $filter_count)
    {
        if (self::is_block($type, $filter_time, $filter_count)) {
            self::abort(429, $source, $source_function);
        } else {
            self::add($type, $level, $message, $source, $source_function);
        }
        return true;
    }
    
    public static function getCount($type, $ip, $filter_time)
    {
        return Log::where('created_at', '>', Carbon::now()->subMinutes($filter_time))
            ->where('type', '=', $type)
            ->where('ip', '=', $ip)
            ->get()
            ->count();
    }
    
    public static function debug($type, $message, $source, $source_function)
    {
        // Detailed debug information
        return self::add($type, 'debug', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function info($type, $message, $source, $source_function)
    {
        // Interesting events. Examples: User logs in, SQL logs.
        return self::add($type, 'info', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function notice($type, $message, $source, $source_function)
    {
        // Notice: normal but significant condition
        return self::add($type, 'notice', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function warning($type, $message, $source, $source_function)
    {
        // Warning: Exceptional occurrences that are not errors.
        // Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong
        return self::add($type, 'warning', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function error($type, $message, $source, $source_function)
    {
        // Runtime errors that do not require immediate action but should typically be logged and monitored.
        return self::add($type, 'error', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function critical($type, $message, $source, $source_function)
    {
        // Critical conditions. Example: Application component unavailable, unexpected exception.
        return self::add($type, 'critical', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function alert($type, $message, $source, $source_function)
    {
        // Alert: Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
        return self::add($type, 'alert', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function emergency($type, $message, $source, $source_function)
    {
        // Emergency: system is unusable
        return self::add($type, 'emergency', $message, $source, $source_function, debug_backtrace());
    }
    
    public static function rotate($days)
    {
        Log::where('created_at', '<', Carbon::now()->subDays($days))->destroy();
    }
    
    public static function abort($code, $source, $source_function)
    {
        self::add('error', 'error', $code . ': ' . \Request::fullUrl(), $source, $source_function, debug_backtrace());
        return app()->abort($code);
    }
}