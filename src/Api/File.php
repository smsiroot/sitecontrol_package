<?php

namespace Sitecontrol;

use Local\Local;

trait File
{
    public function fileCopy()
    {
        // Копирование файла
        $url = urldecode($this->param['url']);
        $path = Local::getUploadPath($this->param['type']);
        $filename = Local::getUploadFileName($url, $this->param['type'], $this->param['alias']);
        $file = Parser::getContent($url);
        if (mb_strlen($file) > 0) {
            if (\Storage::put($path . $filename, $file)) {
                return Local::DOMAIN . Local::UPLOADPATH . $path . $filename;
            }
        }
    }
}