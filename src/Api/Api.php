<?php

namespace Sitecontrol;

use Illuminate\Support\Str;

class Api
{
    use File, Content;
    
    const TOKEN = 'Ab49Kzpqo3XohM6WY';
    public $param;
    
    public function __construct()
    {
        parse_str($_SERVER['QUERY_STRING'], $this->param);
    }
    
    public function index()
    {
        // Вызов метода
        if (self::TOKEN == $this->param['token']) {
            $method = $this->param['target'] . Str::title($this->param['method']);
            if (method_exists($this, $method)) {
                echo $this->$method();
            }
        }
    }
}