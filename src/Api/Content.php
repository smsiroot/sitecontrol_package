<?php

namespace Sitecontrol;

use Local\Local;

trait Content
{
    public function contentAlias()
    {
        $id = $this->param['id'];
        $name = urldecode($this->param['name']);
        $type = $this->param['type'];
        return Local::getAlias($id, $name, $type);
    }
    
    public function contentUrl()
    {
        $id = $this->param['id'];
        $category_id = $this->param['category_id'];
        $name = urldecode($this->param['name']);
        $type = $this->param['type'];
        $alias = $this->param['alias'];
        return Local::getContentUrl($id, $category_id, $name, $type, $alias);
    }
    
    public function categoryUrl()
    {
        $id = $this->param['id'];
        $name = urldecode($this->param['name']);
        $type = $this->param['type'];
        $alias = $this->param['alias'];
        return Local::getCategoryUrl($id, $name, $type, $alias);
    }
}